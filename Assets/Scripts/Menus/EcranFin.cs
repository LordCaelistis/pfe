﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EcranFin : MonoBehaviour {

    Color textColor;
    public Text texteFin;

    static float alphaLevel = 0;
    public static bool FadeIn = false;
    public float step = 0.5f;

    private void Start()
    {
        Destroy(GameObject.Find("Protagoniste"));
        Destroy(GameObject.Find("MainCamera"));
        Time.timeScale = 0.25f;
        FadeIn = true;
    }

    private void Update()
    {
        textColor = new Color(0, 0, 0, alphaLevel);

        texteFin.color = textColor;

        if (FadeIn == true) alphaLevel += Time.deltaTime;
        else alphaLevel -= Time.deltaTime;
        alphaLevel = Mathf.Clamp(alphaLevel, 0, 1);
    }


    public void GoToMenuPrincipal()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }

    public void LeaveGame()
    {
        Application.Quit();
    }
}
