﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPrincipal : MonoBehaviour {

    public GameObject générique;
    public GameObject tutoriel;

    private bool génériqueIsDisplayed = false;
    private bool tutorielIsDisplayed = false;

    private void Update()
    {
        if (Input.GetKeyDown("mouse 0"))
        {
            générique.SetActive(false);
            tutoriel.SetActive(false);
        }
    }

    public void BeginGame()
    {
        SceneManager.LoadScene("CouloirBoss");
    }

    public void LeaveGame()
    {
        Application.Quit();
    }

    public void DisplayTutoriel()
    {
        if (tutorielIsDisplayed) tutoriel.SetActive(false);
        else tutoriel.SetActive(true);
    }

    public void DisplayGénérique()
    {
        if(génériqueIsDisplayed) générique.SetActive(false);
        else générique.SetActive(true);
    }
}
