﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swordScript : MonoBehaviour
{
    //GameSystems Controllers
    [SerializeField]
    public PlayerController playerController;
    public GameObject personnage;
    public bool bossInRange = false;
    public List<GameObject> enemiesInRange = new List<GameObject>();

    private void OnLevelWasLoaded(int level)
    {
        enemiesInRange.Clear();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.layer == 8)
        {
            if (coll.gameObject.name.Contains("Ennemi") == true) enemiesInRange.Add(coll.gameObject);
            else enemiesInRange.Add(coll.gameObject);
        }
    }


    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.layer == 8)
        {
            enemiesInRange.Remove(coll.gameObject);
        }
    }
}