﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour {

    public GameObject playerPrefab;
    public Camera mainCamera;
    private GameObject instancePlayer;
    private Camera instanceCamera;

    // Use this for initialization
    void Awake () {
        //S'il n'y a pas de protag on le fait spawn avec la caméra
        if(GameObject.Find("Protagoniste") == null)
        {
            instancePlayer = Instantiate(playerPrefab, transform.position, transform.rotation);
            instancePlayer.name = "Protagoniste";
            instanceCamera = Instantiate(mainCamera, transform.position, transform.rotation);
            instanceCamera.name = "MainCamera";
        }
        //Sinon on trouve l'objet "Protagoniste" et on le téléporte sur l'objet PlayerSpawn
        else
        {
            instancePlayer = GameObject.Find("Protagoniste");
            instancePlayer.transform.position = transform.position;
            instanceCamera = Camera.main;
            instanceCamera.transform.position = transform.position;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
