﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotBehavior : MonoBehaviour {

    public Animator animator;
    public GameObject tirBlaster;
    private float angle;

    private struct PointInSpace
    {
        public Vector3 Position;
        public float Time;
    }
    private Queue<PointInSpace> pointsInSpace = new Queue<PointInSpace>();
    private float delay = 0.1f;
    private float speed = 5;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 target = new Vector2(transform.parent.transform.position.x, transform.parent.transform.position.y); //vector for transforming the shield to the target, aka the player
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10); //where the mouse is on the screen
        Vector3 lookPos = GameObject.Find("MainCamera").GetComponent<Camera>().ScreenToWorldPoint(mousePos); //position for the shield to look towards
        lookPos = lookPos - transform.position; //make the shield look towards the mouse
        angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg; //turn radians to degrees
        Quaternion angleToRotateTo = Quaternion.AngleAxis(angle, Vector3.forward); //transform the rotation of the shield towards the mouse
        transform.rotation = Quaternion.Slerp(transform.rotation, angleToRotateTo, 0.1f);
        transform.position = Vector3.Slerp(target, transform.position, 0.1f);
        //transform.position = target; //set the position of the shield to the target

        //transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, transform.position.z - 10), pointsInSpace.Dequeue().Position, Time.deltaTime * speed);

        //animator.SetFloat("DirectionX", directionFaced.x);
        //animator.SetFloat("DirectionY", directionFaced.y);
    }

    //Fonctions mathématiques pour repérer position du curseur et viser correctement
    Vector2 GetCursorRelativeVector()
    {
        Vector3 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float relativeX = cursorPos.x - transform.position.x;
        float relativeY = cursorPos.y - transform.position.y;
        return new Vector2(relativeX, relativeY);
    }
    float GetCursorRelativeAngle()
    {
        Vector2 relativeVector = GetCursorRelativeVector();
        return Mathf.Atan2(relativeVector.y, relativeVector.x) * Mathf.Rad2Deg;
    }


    public void Shoot()
    {
        transform.parent.GetComponent<EnergyScript>().energy = GetComponent<EnergyScript>().energy - 1;
        transform.parent.GetComponent<EnergyScript>().beginEnergyRegen();

        //Récupération de la position de la souris par rapport à l'écran
        Vector3 pz = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,
                        Input.mousePosition.y, Camera.main.transform.position.y - transform.position.y));
        Vector3 direction = pz - transform.position;

        direction.z = 0;
        //Normalize() impose une longueur de 1 au vecteur. Sinon, la vitesse du projectile est modifiée
        //selon que la souris soit plus ou moins loin
        direction.Normalize();
        Vector3 pos0 = transform.position;
        Quaternion rot0 = Quaternion.Euler(0, 0, GetCursorRelativeAngle() - 90);
        GameObject projectile = Instantiate(tirBlaster, pos0, rot0);
        projectile.GetComponent<BulletBehavior>().SetBulletDamage(PlayerController.blasterDamage);
        projectile.GetComponent<Rigidbody2D>().velocity = direction * PlayerController.tirBlasterSpeed;
    }

}
