﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour {

    public AudioSource SourceSon;
    public AudioClip[] Desint;
    public AudioClip[] ImpactEnnemis;
    public AudioClip[] ImpactBois;
    public AudioClip[] ImpactMetal;
    public AudioClip[] ImpactRoche;

    public static float bulletDamage;

    void Start()
    {
        Physics2D.IgnoreCollision(GameObject.Find("Protagoniste").GetComponent<Collider2D>(), GetComponent<Collider2D>());
        Physics2D.IgnoreLayerCollision(0, 9, true);
    }

    private void Update()
    {
        //if (GetComponent<Rigidbody2D>().velocity != startingVelocity) Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        SoundManager.PlayRandomOneShot(SourceSon, Desint, 0.2f, 0.3f, 0.8f, 1.2f, 1f);
        
        //Destruction de la balle si elle rencontre le boss ou un mur
        if (col.gameObject.layer != 0)
        {
            Destroy(gameObject);
            if (col.gameObject.layer != 9)
            {
                if (col.gameObject.tag == "Ennemis")
                {
                    SoundManager.PlayRandomOneShot(SourceSon, ImpactEnnemis, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
                }
                if (col.gameObject.tag == "Bois") SoundManager.PlayRandomOneShot(SourceSon, ImpactBois, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
                if (col.gameObject.tag == "Roche") SoundManager.PlayRandomOneShot(SourceSon, ImpactRoche, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
                if (col.gameObject.tag == "Metal") SoundManager.PlayRandomOneShot(SourceSon, ImpactMetal, 0.2f, 0.3f, 0.9f, 1.1f, 1f);

                Destroy(gameObject);

                if (col.gameObject.layer == 8)
                {
                    col.gameObject.GetComponent<EnemyController>().TakeDamage(bulletDamage);                    
                }
            }
        }
    }
        
        //Fonction appelée par PlayerController pour définir les dégâts du blaster selon l'arme actuellement dans l'inventaire
    public void SetBulletDamage(float bulletDamageValue)
    {
        bulletDamage = bulletDamageValue;
    }
}
