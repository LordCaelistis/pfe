﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventaireItems : MonoBehaviour {

    public QuestItem[] questItems = new QuestItem[4];
    public Canvas canvasBûches;
    public Text textZoneBûches;
    public Canvas canvasJunk;
    public Text textZoneJunk;
    public static Canvas canvasPotions;
    public static Text textZonePotions;

    public int nbBûches = 0;
    [SerializeField]
    public static int nbPotions = 0;

    private void Start()
    {
        for(int i = 0; i < questItems.Length; i++)
        {
            questItems[i].currentAmount = 0;
        }
    }

    private void Update()
    {
        if (nbBûches == 6)
        {
            AvancementQuete.BoisCoupe1 = true;
        }
        if (nbBûches == 7 && AvancementQuete.BoisCoupe1 == true)
        {
            AvancementQuete.BoisCoupe2 = true;
        }
        if (nbBûches == 12 && AvancementQuete.BoisCoupe2 == true)
        {
            AvancementQuete.BoisCoupe3 = true;
        }
    }

    public void GetBûche()
    {
        for(int i = 0; i < questItems.Length; i++)
        {
            if (questItems[i].nom == "Bûche")
            {
                nbBûches += 1;
                canvasBûches = GameObject.Find("Canvas Bûches").GetComponent<Canvas>();
                textZoneBûches = GameObject.Find("NombreBûches").GetComponent<Text>();
                textZoneBûches.text = nbBûches.ToString();                
            }
        }
        if (canvasBûches.GetComponent<CanvasGroup>().alpha == 0) canvasBûches.GetComponent<CanvasGroup>().alpha = 1;
    }

    public void GetJunk()
    {
        if (canvasJunk == null)
        {
            canvasJunk = GameObject.Find("Canvas Junk").GetComponent<Canvas>();
            textZoneJunk = GameObject.Find("NombreJunk").GetComponent<Text>();
        }
        if (canvasJunk.GetComponent<CanvasGroup>().alpha == 0) canvasJunk.GetComponent<CanvasGroup>().alpha = 1;
        AvancementQuete.JunkTrouve += 1;
        textZoneJunk.text = AvancementQuete.JunkTrouve.ToString();
    }

    public static void GetPotions()
    {
        nbPotions = 2;
        canvasPotions = GameObject.Find("Canvas Potions").GetComponent<Canvas>();
        textZonePotions = GameObject.Find("NombrePotions").GetComponent<Text>();
        canvasPotions.GetComponent<CanvasGroup>().alpha = 1;
        textZonePotions.text = nbPotions.ToString();
    }

    //Fonction pour vider les bûches quand on les donne aux PNJ
    public void GiveBûches(int nBBuchesDonnees)
    {
        nbBûches -= nBBuchesDonnees;
        textZoneBûches = GameObject.Find("NombreBûches").GetComponent<Text>();
        textZoneBûches.text = nbBûches.ToString();
    }

    public void GiveJunk()
    {
        AvancementQuete.JunkTrouve = 0;
        canvasJunk = GameObject.Find("Canvas Junk").GetComponent<Canvas>();
        canvasJunk.GetComponent<CanvasGroup>().alpha = 0;
    }
}
