﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour {

    public int health;
    public int maxHealth;

    public AudioSource SourceSonDegat;
    public AudioSource SourceSonDegatCri;
    //DegatPersonnage
    public AudioClip[] DegatRecu;
    public AudioClip[] CrisPerso;

    public Image[] hearts;
    public Sprite FullHeart;
    public Sprite EmptyHeart;

    private IEnumerator coroutine;
    public float duréeHitstun = 0.4f;

    // Update is called once per frame
    void Update () {

        for (int i = 0; i < hearts.Length; i++)
        {
                //Affiche un coeur vide ou plein selon la santé actuelle
            if (i < health) hearts[i].sprite = FullHeart;
            else hearts[i].sprite = EmptyHeart;

            //Affiche un nombre de coeurs correspondant à la santé max renseignée
            if (i < maxHealth) hearts[i].enabled = true;
            else hearts[i].enabled = false;
        }

    }

    public void TakeDamage()
    {
        PlayerController.Instance.animator.SetTrigger("Hit");
        StartCoroutine(Camera.main.GetComponent<CameraControl>().ShakeCamera(0.2f));

        SoundManager.PlayRandomOneShot(SourceSonDegat, DegatRecu, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
        SoundManager.PlayRandomOneShot(SourceSonDegatCri, CrisPerso, 0.2f, 0.3f, 0.9f, 1.1f, 0.4f);

        health -= 1;
            if (health == 0) {
                IEnumerator coroutineLocale = PlayerController.Instance.Death();
                StartCoroutine(coroutineLocale);
            }
    }

    public void RefillHealth()
    {
        health = maxHealth;
    }
}
