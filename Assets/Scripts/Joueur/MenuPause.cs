﻿using UnityEngine;
using System.Collections;

public class MenuPause : MonoBehaviour
{
    private bool paused = false;
    public GameObject menuPause;

    public void Update()
    {
        //Menu Pause
        if (Input.GetButtonDown("Pause"))
        {
            Pause();
        }
    }
    
    public void Pause()
    {
            //On stoppe le temps in-game et on fait apparaître le menu de pause
        if (!paused)
        {
            Time.timeScale = 0;
            paused = true;
            menuPause.SetActive(true);
        }   
            //On fait repartir le temps et on cache le menu de pause
        else
        {
            Time.timeScale = 1;
            paused = false;
            menuPause.SetActive(false);
        }
    }

        //Quitte le jeu ou arrête de jouer dans l'éditeur Unity le cas échéant
    public void LeaveGame()
    {
        Application.Quit();
    }
}
