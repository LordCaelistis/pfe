﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventaire : MonoBehaviour {

    public static Inventaire instance = null;
    public const int nbItemSlots = 4;
    public Image[] weaponImages = new Image[nbItemSlots];
    public Weapon[] weapons = new Weapon[nbItemSlots];
    public List<QuestItem> questItems;

    public static int nbBûches = 0;
    public Canvas canvasBûches;
    public Text textZone;

    void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);
    }

    public Weapon GetWeaponByType(string typeToCheck)
    {
        Weapon tempWeapon = null;
        for (int i = 0; i < weapons.Length; i++)
        {
            //print(weapons[i]);
            if (weapons[i].itemName.Contains(typeToCheck))
            {
                tempWeapon = weapons[i];
                break;
            }
        }
        return tempWeapon;
    }

    public void ChangeWeapon (Weapon weaponToAdd, int arraySlotToUse)
    {
        weapons[arraySlotToUse] = weaponToAdd;
        weaponImages[arraySlotToUse].sprite = weaponToAdd.sprite;
        weaponImages[arraySlotToUse].enabled = true;
    }

    public void DisplayInventaire()
    {
        GetComponent<Canvas>().enabled = true;
    }

    public void HideInventaire()
    {
        GetComponent<Canvas>().enabled = false;
    }

    /*public void RemoveWeapon(string weaponToRemove)
    {
        for (int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i].itemName == weaponToRemove)
            {
                print("Coucou");
                weapons[i] = null;
                weaponImages[i].sprite = null;
                weaponImages[i].enabled = false;
                return;
            }
        }
    }*/
}
