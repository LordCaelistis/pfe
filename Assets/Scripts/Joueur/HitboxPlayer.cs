﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxPlayer : MonoBehaviour {

    public Animator animator;
    public GameObject reflectedProjectile;
    public float projectileSpeed = 18f;
    public float duréeHitstun = 0.4f;

    //Fonctions mathématiques pour repérer position du curseur et viser correctement
    Vector2 GetCursorRelativeVector()
    {
        Vector3 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float relativeX = cursorPos.x - transform.position.x;
        float relativeY = cursorPos.y - transform.position.y;
        return new Vector2(relativeX, relativeY);
    }
    float GetCursorRelativeAngle()
    {
        Vector2 relativeVector = GetCursorRelativeVector();
        return Mathf.Atan2(relativeVector.y, relativeVector.x) * Mathf.Rad2Deg;
    }

    //Fonction projetant un projectile lors de la contre-attaque
    void CounterAttack(GameObject attacker, bool isProjectile)
    {
        if(isProjectile == true)
        {
            //Récupération de la position de la souris par rapport à l'écran
            Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pz.z = 0;
            Vector3 direction = pz - transform.position;
            direction.z = 0;
            //Normalize() impose une longueur de 1 au vecteur. Sinon, la vitesse du projectile est modifiée
            //selon que la souris soit plus ou moins loin
            direction.Normalize();

            Vector3 pos0 = transform.position;
            Quaternion rot0 = Quaternion.Euler(0, 0, GetCursorRelativeAngle() - 90);
            GameObject projectile = Instantiate(reflectedProjectile, pos0, rot0);
            //projectile.GetComponent<ReflectedProjectileBehavior>().SetProjectileDamage(blasterDamage);
            projectile.GetComponent<Rigidbody2D>().velocity = direction * projectileSpeed;
            animator.SetTrigger("Slash");
        }        
    }

    IEnumerator OnTriggerEnter2D(Collider2D col)
    {
        //Si le joueur n'est pas invincible et qu'il ne contre pas, bobo
        if (GameObject.Find("Protagoniste").GetComponent<HealthScript>().health > 0)
        {
            if (!PlayerController.invincible && !PlayerController.contre)
            {
                if (col.tag == "Projectiles")
                {
                    PlayerController.invincible = true;
                    PlayerController.freezeMovement(duréeHitstun);
                    GameObject.Find("Protagoniste").GetComponent<HealthScript>().TakeDamage();
                    IEnumerator coroutine = Camera.main.GetComponent<CameraControl>().ShakeCamera(duréeHitstun);
                    StartCoroutine(coroutine);
                    yield return new WaitForSeconds(PlayerController.invincibleDurée);
                    PlayerController.invincible = false;
                }
            }
        }
        //Si le joueur n'est pas invincible et qu'il est en train de contrer, on devient invincible et on contre-attaque
        else if (!PlayerController.invincible && PlayerController.contre)
        {
            if (col.tag == "Projectiles")
            {
                Destroy(col.gameObject);
                CounterAttack(col.gameObject, true);
                PlayerController.invincible = true;
                PlayerController.contre = false;
                yield return new WaitForSeconds(PlayerController.invincibleDurée);
                PlayerController.invincible = false;
            }
        }
    }
}
