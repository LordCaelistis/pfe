﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockageArmes : MonoBehaviour {

    public static StockageArmes instanceStockageArmes = null;
    public Weapon[] armesStockées = new Weapon[12];

    void Awake()
    {
        if (instanceStockageArmes == null) instanceStockageArmes = this;
        else if (instanceStockageArmes != this) Destroy(gameObject);
    }

    public void SetWeapon(string nameWeaponToAdd)
    {
        for (int i = 0; i < armesStockées.Length; i++)
        {
            if (armesStockées[i].itemName == nameWeaponToAdd)
            {
                if (armesStockées[i].itemName.Contains("Blaster"))
                {
                    Inventaire.instance.ChangeWeapon(armesStockées[i], 0);
                }
                else if (armesStockées[i].itemName.Contains("Melee"))
                {
                    Inventaire.instance.ChangeWeapon(armesStockées[i], 1);
                }
            }
        }
    }

}
