 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Header("Audio")]
    public AudioSource SourceSon;
    public AudioSource SourceSon2;
    public AudioSource SourceSon3;
    public AudioSource SourceSon4;
    // Paramètres : Nom de la source | Nom de la liste de sons | Volume min | Volume max | Pitch min | Pitch max | Proba
    // SoundManager.PlayRandomOneShot(soundSource, soundList, 0.8f, 1f, 0.9f, 1.1f, 1f);

    //Inventaire
    public AudioClip[] InventaireOuverture;
    public AudioClip[] InventaireFermeture;

        //Action
    public AudioClip[] DashPerso;
    public AudioClip[] HealPerso;

        //AttaquePersonnage
    public AudioClip[] SwooshPersoEpee;
    public AudioClip[] CrisAttaque;
    public AudioClip[] TirPerso;
    public AudioClip[] ImpactLazerBasique;
    public AudioClip[] ImpactLazerEnnemis;
    public AudioClip[] ImpactLazerBois;
    public AudioClip[] ImpactLazerMetal;
    public AudioClip[] ImpactLazerRoche;
    public AudioClip[] ImpactEpee;
    public AudioClip[] Contre;

        //Footsteps
    public AudioClip[] FootstepsHerbe;
    public AudioClip[] FootstepsTerre;
    public AudioClip[] FootstepsBoue;
    public AudioClip[] FootstepsBeton;
    public AudioClip[] FootstepsBois;
    public AudioClip[] FootstepsMetal;

    public float cooldown_footsteps_max = 0.3f;
    float cooldown_footsteps_now = 0f;

    public static PlayerController Instance { get; private set; }

    [Header("BOT")]
    public GameObject bauoteuxPrefab;
    GameObject bauoteux;
    BauoteuxScript bauoteuxScript;
    public GameObject bauoteuxPosition;

    [Header("Objets in-game")]
    public Rigidbody2D playerRigidbody2D;
    public float playerRigidbody2DInitialDrag;
    public SpriteRenderer sprite;
    public Animator animator;
    [SerializeField] GameObject tirBlaster;
    public Rigidbody2D tirBlasterPhysics;
    public GameObject swordHitbox;
    public GameObject shootingDirection;
    public GameObject deathScreenPrefab;

    [SerializeField]
    public static float vitesse = 13f;
    public static float vitesseBase = 13f;
    public float vitesseDash = 50f;
    public float duréeDash = 0.3f;
    public static float tirBlasterSpeed = 30f;
    public float healDurée = 1f;

    //PNJ
    public TalkWithContremaitre contremaitre;
    public TalkWithForgeron forgeron;
	public TalkWithApothicaire apothicaire;

    //Cooldowns
    [SerializeField] public static float cooldown_max_move = 0.4f;
    public static float cooldown_now_move = 0f;
    public float cooldown_max_dash = 0.5f;
    public static float cooldown_now_dash = 0f;
    public float cooldown_max_shoot = 0.15f;
    static float cooldown_now_shoot = 0f;
    public float cooldown_max_strike = 1f;
    static float cooldown_now_strike = 0f;
    public float cooldown_max_contre = 2f;
    static float cooldown_now_slow = 0f;
    public float cooldown_max_slow = 0.4f;
    static float cooldown_now_combo = 0f;
    float cooldown_now_contre = 0f;
    [SerializeField] public static float cooldown_energie = 0.8f;
    private int etapeCombo = 0;

        //Dégâts
    float meleeDamage;
    public static float blasterDamage;

        //Coûts en énergie
    public static int blasterEnergyCost = 1;
    int dashEnergyCost = 1;

    public static bool invincible = false;
    public static float invincibleDurée = 1f;
    public static bool contre = false;
    public static float contreDurée = 0.3f;
    private float contreFreezeDurée = 0.8f;
    private bool actionsAreFrozen = false;
    public static bool canMoveHorizontal = true;
    public static bool canMoveVertical = true;
    bool canPlayMovementAnimation = true;

    //Direction regardée par le joueur (pour dash et attaque mêlée)
    private Vector2 directionFaced = Vector2.right;

    private IEnumerator coroutine;
    public static string PrevScene;

    void Awake()
    {
        Instance = this;
    }

    private void OnLevelWasLoaded(int level)
    {
        DeathScreen.FadeIntoScene();

        //Checker de quelle scène on vient pour faire correctement pop le joueur dans le village quand on vient de la forêt
        if(PlayerPrefs.HasKey("SceneName")) PrevScene = PlayerPrefs.GetString("SceneName");
        PlayerPrefs.SetString("SceneName", SceneManager.GetActiveScene().name);
        if(PrevScene == "Foret_Polluee_V2" && GameObject.Find("PlayerSpawnFromForet"))
        {
            transform.position = GameObject.Find("PlayerSpawnFromForet").transform.position;
            Camera.main.transform.position = GameObject.Find("PlayerSpawnFromForet").transform.position;
        }
        if (PrevScene == "Village" && GameObject.Find("PlayerSpawnFromVillage"))
        {
            transform.position = GameObject.Find("PlayerSpawnFromVillage").transform.position;
            Camera.main.transform.position = GameObject.Find("PlayerSpawnFromVillage").transform.position;
        }
        if (PrevScene == "SceneBoss" && GameObject.Find("PlayerSpawn"))
        {
            transform.position = GameObject.Find("PlayerSpawn").transform.position;
            Camera.main.transform.position = GameObject.Find("PlayerSpawn").transform.position;
        }

        bauoteux = Instantiate(bauoteuxPrefab, bauoteuxPosition.transform.position, Quaternion.Euler(0, 0, 0));
        bauoteuxScript = bauoteux.GetComponent<BauoteuxScript>();
        bauoteuxScript.protagoniste = gameObject;
        bauoteuxScript.targetPosition = bauoteuxPosition;
    }

    // Use this for initialization
    void Start()
    {
        bauoteux = Instantiate(bauoteuxPrefab, bauoteuxPosition.transform.position, Quaternion.Euler(0, 0, 0));
        bauoteuxScript = bauoteux.GetComponent<BauoteuxScript>();
        bauoteuxScript.protagoniste = gameObject;
        bauoteuxScript.targetPosition = bauoteuxPosition;

        StockageArmes.instanceStockageArmes.SetWeapon("Blaster1");
        StockageArmes.instanceStockageArmes.SetWeapon("Melee1");
        DontDestroyOnLoad(this.gameObject);

        playerRigidbody2DInitialDrag = playerRigidbody2D.drag;

        //Initialisation blaster
        Weapon currentBlaster = Inventaire.instance.GetWeaponByType("Blaster");
        blasterDamage = currentBlaster.weaponDamage;
        /*cooldown_max_shoot = currentBlaster.cooldown;
        projectileSpeed = currentBlaster.projectileSpeed;*/

            //Initialisation melee
        Weapon currentMelee = Inventaire.instance.GetWeaponByType("Melee");
        meleeDamage = currentMelee.weaponDamage;
    }

        // Update is called once per frame
    void Update()
    {
            //Orientation de l'épée selon la direction regardée
        swordHitbox.transform.rotation = Quaternion.Euler(0, 0,
            Mathf.Round(Vector2.SignedAngle(Vector2.right, directionFaced)/90)*90
        );
        shootingDirection.transform.rotation = Quaternion.Euler(0, 0, GetCursorRelativeAngle());

        if (cooldown_footsteps_now < cooldown_footsteps_max) cooldown_footsteps_now += Time.deltaTime;

        if (cooldown_now_move <= 0f && (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0))
        {
            if (canMoveHorizontal == true & canMoveVertical == true) playerRigidbody2D.velocity += new Vector2(Input.GetAxis("Horizontal") * vitesse, Input.GetAxis("Vertical") * vitesse);
            else if (canMoveHorizontal == true & canMoveVertical == false) playerRigidbody2D.velocity += new Vector2(Input.GetAxis("Horizontal") * vitesse, 0);
            else if (canMoveHorizontal == false & canMoveVertical == true) playerRigidbody2D.velocity += new Vector2(0, Input.GetAxis("Vertical") * vitesse);
            directionFaced.x = Input.GetAxis("Horizontal");
            directionFaced.y = Input.GetAxis("Vertical");

            if(cooldown_footsteps_now >= cooldown_footsteps_max)
            {
                // Paramètres : Nom de la source | Nom de la liste de sons | Volume min | Volume max | Pitch min | Pitch max | Proba
                if (SceneManager.GetActiveScene().name == "SceneBoss" || SceneManager.GetActiveScene().name == "CouloirBoss")          
                    SoundManager.PlayRandomOneShot(SourceSon3, FootstepsBeton, 0.05f, 0.1f, 0.9f, 1.1f, 1f);
                else
                    SoundManager.PlayRandomOneShot(SourceSon3, FootstepsHerbe, 0.05f, 0.1f, 0.9f, 1.1f, 1f);

                cooldown_footsteps_now -= cooldown_footsteps_max;
            }
            
        }
        directionFaced = directionFaced.normalized;

            //Limite la vitesse du joueur
        playerRigidbody2D.velocity = new Vector2(Mathf.Clamp(playerRigidbody2D.velocity.x, -vitesse, vitesse), Mathf.Clamp(playerRigidbody2D.velocity.y, -vitesse, vitesse));
        if (cooldown_now_slow > 0) playerRigidbody2D.velocity = playerRigidbody2D.velocity * 0.5f;

        if (SceneManager.GetActiveScene().name != "Village")
        {
            if (Input.GetButtonDown("Melee") && cooldown_now_strike <= 0)
            {
                    // Paramètres : Nom de la source | Nom de la liste de sons | Volume min | Volume max | Pitch min | Pitch max | Proba
                 SoundManager.PlayRandomOneShot(SourceSon2, CrisAttaque, 0.3f, 0.4f, 0.9f, 1.1f, 0.2f);
                SoundManager.PlayRandomOneShot(SourceSon, SwooshPersoEpee, 0.3f, 0.4f, 0.9f, 1.1f, 1f);
                if (etapeCombo < 2)
                {
                    cooldown_now_strike += 0.2f;
                    cooldown_now_combo = cooldown_max_strike * 1.2f;
                }
                else
                {
                    cooldown_now_strike += cooldown_max_strike;
                    cooldown_now_combo = 0;
                }
                StartCoroutine(Strike());
            }

            if (cooldown_now_combo <= 0) etapeCombo = 0; 
                //Tire dans la direction indiquée
            if (Input.GetButtonDown("Blaster") && cooldown_now_shoot <= 0 && GetComponent<EnergyScript>().energy > 0)
            {
                    // Paramètres : Nom de la source | Nom de la liste de sons | Volume min | Volume max | Pitch min | Pitch max | Proba
                SoundManager.PlayRandomOneShot(SourceSon, TirPerso, 0.3f, 0.3f, 0.8f, 1.2f, 1f);
                cooldown_now_shoot += cooldown_max_shoot;
                if (canMoveHorizontal && canMoveVertical) cooldown_now_slow += cooldown_max_slow;
                bauoteux.GetComponent<BauoteuxScript>().Shoot();
            }
        }

            //Dash
        if (Input.GetButtonDown("Dash") && cooldown_now_dash <= 0 /*&& GetComponent<EnergyScript>().energy > 0*/)
        {
            cooldown_now_dash += cooldown_max_dash;
            cooldown_now_contre += duréeDash;
            cooldown_now_slow = 0;

            coroutine = Dash();
            StartCoroutine(coroutine);
        }

            //Guérison
        if (Input.GetButtonDown("Heal") && GetComponent<HealthScript>().health < GetComponent<HealthScript>().maxHealth && InventaireItems.nbPotions > 0)
        {
            InventaireItems.nbPotions -= 1;
            GameObject.Find("NombreJunk").GetComponent<Text>().text = InventaireItems.nbPotions.ToString();
            SoundManager.PlayRandomOneShot(SourceSon, HealPerso, 0.8f, 1f, 0.9f, 1.1f, 1f);
            coroutine = Heal();
            StartCoroutine(coroutine);
        }

            //Contre
        /*if (Input.GetButtonDown("ContreParfait") && cooldown_now_contre <= 0 && GetComponent<EnergyScript>().energy > 0)
        {
            SoundManager.PlayRandomOneShot(SourceSon, Contre, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
            cooldown_now_contre += cooldown_max_contre;
            coroutine = ContreParfait();
            StartCoroutine(coroutine);
        }*/

        //Inventaire
                   // Paramètres : Nom de la source | Nom de la liste de sons | Volume min | Volume max | Pitch min | Pitch max | Proba
        /*if (Input.GetButtonDown("Inventaire")) SoundManager.PlayRandomOneShot(SourceSon, InventaireOuverture, 0.4f, 0.5f, 0.9f, 1f, 1f);
        if (Input.GetButtonUp("Inventaire")) SoundManager.PlayRandomOneShot(SourceSon, InventaireFermeture, 0.4f, 0.5f, 0.9f, 1f, 1f);
        if (Input.GetButton("Inventaire"))
        {
            Inventaire.instance.DisplayInventaire();
        }
        else Inventaire.instance.HideInventaire();*/

            //Respawn au dernier PlayerSpawn
        if (Input.GetKeyDown("b"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

            //Mort instantanée (pour débug)
        if (Input.GetKeyDown("p"))
        {
            Respawn();
        }

        if (GetComponent<HealthScript>().health == 0)
        {
            Death();
        }

        if (cooldown_now_move > 0) cooldown_now_move -= Time.deltaTime;
        if (cooldown_now_strike > 0) cooldown_now_strike -= Time.deltaTime;
        if (cooldown_now_dash > 0) cooldown_now_dash -= Time.deltaTime;
        if (cooldown_now_shoot > 0) cooldown_now_shoot -= Time.deltaTime;
        if (cooldown_now_contre > 0) cooldown_now_contre -= Time.deltaTime;
        if (cooldown_now_slow > 0) cooldown_now_slow -= Time.deltaTime;
        if (cooldown_now_combo > 0) cooldown_now_combo -= Time.deltaTime;

        if (canPlayMovementAnimation == true)
        {
            animator.SetFloat("SpeedX", playerRigidbody2D.velocity.x);
            animator.SetFloat("SpeedY", playerRigidbody2D.velocity.y);
            animator.SetFloat("DirectionX", directionFaced.x);
            animator.SetFloat("DirectionY", directionFaced.y);

        }
    }

    IEnumerator Dash()
    {
        SoundManager.PlayRandomOneShot(SourceSon4, DashPerso, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
        animator.SetBool("isDashing", true);

        //cooldown_now_move += cooldown_max_move;

        /*GetComponent<EnergyScript>().energy = GetComponent<EnergyScript>().energy - dashEnergyCost;
        GetComponent<EnergyScript>().beginEnergyRegen();*/

        invincible = true;
        cooldown_now_move = 1f;

        playerRigidbody2D.velocity = directionFaced * vitesseDash * 2;
        playerRigidbody2D.drag = 0;
        yield return new WaitForSeconds(duréeDash);

        animator.SetBool("isDashing", false);
        playerRigidbody2D.drag = playerRigidbody2DInitialDrag;

        yield return new WaitForSeconds(0.2f);
        playerRigidbody2D.velocity = Vector3.zero;

        invincible = false;
        cooldown_now_move = 0;
    }

    IEnumerator Strike()
    {
        switch (etapeCombo)
        {
            case 0:
                animator.SetTrigger("Slash");
                etapeCombo++;
                break;
            case 1:
                animator.SetTrigger("SecondSlash");
                etapeCombo++;
                break;
            case 2:
                animator.SetTrigger("Slash");
                etapeCombo = 0;
                break;
        }

        cooldown_now_move = 1f;
        playerRigidbody2D.velocity = directionFaced * vitesseDash;
        playerRigidbody2D.drag = 0;
        canPlayMovementAnimation = false;

        yield return new WaitForSeconds(duréeDash/2);

        playerRigidbody2D.drag = playerRigidbody2DInitialDrag;
        playerRigidbody2D.velocity = Vector3.zero;
        canPlayMovementAnimation = true;
        cooldown_now_move = 0.2f;
        

            //On vérifie quels ennemis sont présents dans la HitboxSword puis on leur inflige les dégâts de l'épée
        foreach(GameObject ennemi in swordHitbox.GetComponent<swordScript>().enemiesInRange)
        {
            ennemi.GetComponent<EnemyController>().TakeDamage(meleeDamage);
            //ennemi.GetComponent<Rigidbody2D>().velocity = -directionFaced;
            SoundManager.PlayRandomOneShot(SourceSon, ImpactEpee, 0.4f, 0.5f, 0.9f, 1.1f, 1f);
        }
    }

    //Fonctions mathématiques pour repérer position du curseur et viser correctement
    Vector2 GetCursorRelativeVector()
    {
        Vector3 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float relativeX = cursorPos.x - transform.position.x;
        float relativeY = cursorPos.y - transform.position.y;
        return new Vector2(relativeX, relativeY);
    }
    float GetCursorRelativeAngle()
    {
        Vector2 relativeVector = GetCursorRelativeVector();
        return Mathf.Atan2(relativeVector.y, relativeVector.x) * Mathf.Rad2Deg;
    }    

    IEnumerator Heal()
    {
        freezeMovement(healDurée);
        yield return new WaitForSeconds(healDurée);
        GetComponent<HealthScript>().health += 1;
    }

    IEnumerator ContreParfait()
    {
            //On empêche le joueur d'effectuer des actions pdt qu'il contre
        freezeMovement(contreFreezeDurée);
        contre = true;
        yield return new WaitForSeconds(contreDurée);
        contre = false;
    }

        //On reset la vélocité du joueur et on appelle Respawn()
    public IEnumerator Death()
    {
        invincible = true;
        freezeMovement(2f);
        DeathScreen.FadeIn = true;
        animator.SetBool("Dead", true);
        sprite.sortingLayerName = "Overlay";
        sprite.sortingOrder = 11000;
        playerRigidbody2D.velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(3f);
        DeathScreen.FadeIn = false;
        sprite.sortingOrder = 0;
        sprite.sortingLayerName = "Default";

        foreach (GameObject ennemi in GameObject.FindGameObjectsWithTag("Ennemis"))
        {
            Destroy(ennemi);
        }
        foreach (GameObject projectile in GameObject.FindGameObjectsWithTag("Projectiles"))
        {
            Destroy(projectile);
        }
        Respawn();
    }

        //On re-remplit la santé du joueur et on le TP au point approprié selon la scène
    public void Respawn()
    {
        GetComponent<HealthScript>().RefillHealth();
        animator.SetBool("Dead", false);
        if (SceneManager.GetActiveScene().name != "SceneBoss")
        {
            GameObject respawnPoint = GameObject.Find("PlayerSpawn");
            transform.position = respawnPoint.transform.position;
            invincible = false;
        }
        else
        {
            SceneManager.LoadScene("Maison");
        }
        invincible = false;
    }

        //Fonction pour bloquer les mouvements du joueur si besoin est (contre, cutscene...)
    public static void freezeMovement(float freezeDurée)
    {
        cooldown_now_move = freezeDurée;
        cooldown_now_dash = freezeDurée;
        cooldown_now_shoot = freezeDurée;
        cooldown_now_strike = freezeDurée;
    }

        //Fonction appelée par PNJController pour empêcher le joueur d'attaquer quand il parle à un PNJ
    public void freezeActions()
    {
        if (!actionsAreFrozen)
        {
            cooldown_now_dash += 5000;
            cooldown_now_shoot += 5000;
            cooldown_now_strike += 5000;
            actionsAreFrozen = true;
        }
        else
        {
            cooldown_now_dash = 0;
            cooldown_now_shoot = 0;
            cooldown_now_strike = 0;
            actionsAreFrozen = false;
        }
    }

    public void ContremaitreQueteAcceptee()
    {
        contremaitre.QueteAcceptee();
    }

    public void ContremaitreQueteRefusee()
    {
        contremaitre.QueteRefusee();
    }

    public void ContremaitreImpatienceOui()
    {
        contremaitre.ImpatienceOui();
    }

    public void ContremaitreImpatienceNon()
    {
        contremaitre.ImpatienceNon();
    }

    public void ContremaitreJunkTrouve()
    {
        contremaitre.DechetTrouve();
    }

    public void ContremaitreImpatience2Oui()
    {
        contremaitre.Impatience2Oui();
    }

    public void ContremaitreImpatience3Oui()
    {
        contremaitre.Impatience3Oui();
    }
	
    public void ContremaitreForetEnDanger()
    {
        contremaitre.ForetEnDanger();
    }
    
    public void ForgeronForgerOutil()
    {
        forgeron.ForgerOutil();
    }

    public void ForgeronRefuserOutil()
    {
        forgeron.RefuserOutil();
    }
	
	public void ForgeronReparerLevier()
    {
        forgeron.ReparerLevier();
    }
	
	public void ApothicaireLore1()
    {
        apothicaire.Lore1();
    }
	
	public void ApothicaireLore2()
    {
        apothicaire.Lore2();
    }
	
	public void ApothicaireLore3()
    {
        apothicaire.Lore3();
    }
	
	public void ApothicaireLoreNon()
    {
        apothicaire.LoreNon();
    }
    /*IEnumerator OnCollisionEnter2D(Collision2D col)
    {

        if (!invincible)
        {
            if (col.collider.tag == "Boss")
            {
                GetComponent<HealthScript>().TakeDamage();
                invincible = true;
                yield return new WaitForSeconds(invincibleDurée);
                invincible = false;
            }
        }
    }*/

}
