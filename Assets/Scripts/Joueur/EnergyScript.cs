﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyScript : MonoBehaviour {

    [SerializeField]
    public int energy;
    [SerializeField]
    public int maxEnergy;

    public Image[] energyCells = new Image[10];
    public Sprite FullEnergyCell;
    public Sprite EmptyEnergyCell;

    public static float cooldown_energie_max;
    float cooldown_energie_now = 0f;
    bool reloading = false;

    void Start()
    {
        cooldown_energie_max = PlayerController.cooldown_energie;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < energyCells.Length; i++)
        {
            //Affiche un coeur vide ou plein selon la santé actuelle
            if (i < energy) energyCells[i].sprite = FullEnergyCell;
            else energyCells[i].sprite = EmptyEnergyCell;

            //Affiche un nombre de coeurs correspondant à la santé max renseignée
            if (i < maxEnergy) energyCells[i].enabled = true;
            else energyCells[i].enabled = false;
        }

        //Quand le cooldown atteint 0, le joueur regagne 1 énergie
        if (cooldown_energie_now <= 0 && reloading == true)
        {
            energy += 1;
            if (energy < maxEnergy) cooldown_energie_now += cooldown_energie_max;
            else reloading = false;
        }
        if (cooldown_energie_now > 0) cooldown_energie_now -= Time.deltaTime;
    }

    //Fonction appelée par PlayerController pour enclencher la régen d'énergie. Assure qu'il y a un délai entre conso d'énergie et régen
    public void beginEnergyRegen()
    {
        cooldown_energie_now += cooldown_energie_max;
        reloading = true;
    }
}
