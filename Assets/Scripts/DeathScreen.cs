﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScreen : MonoBehaviour {

    Color screenColor;

    static float alphaLevel = 0;

    public static bool FadeIn = false;
    public float step = 0.5f;

	// Use this for initialization
	void Start () {
        FadeIn = false;
	}
	
	// Update is called once per frame
	void Update () {

        screenColor = new Color(0, 0, 0, alphaLevel);

        GetComponent<SpriteRenderer>().color = screenColor;
        GetComponent<SpriteRenderer>().sortingLayerName = "Overlay";
        GetComponent<SpriteRenderer>().sortingOrder = 10999;

        if (FadeIn == true) alphaLevel += Time.deltaTime;
        else alphaLevel -= Time.deltaTime;
        alphaLevel = Mathf.Clamp(alphaLevel, 0, 1);
	}

    public static void FadeIntoScene()
    {
        alphaLevel = 1;
    }
}
