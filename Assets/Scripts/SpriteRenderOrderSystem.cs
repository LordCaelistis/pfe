﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRenderOrderSystem : MonoBehaviour {

    SpriteRenderer[] spriteRenderers;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		spriteRenderers = FindObjectsOfType<SpriteRenderer>();

        foreach(SpriteRenderer renderer in spriteRenderers)
        {
            if(renderer.sortingLayerName == "Default") renderer.sortingOrder = (int)(renderer.transform.position.y * -100);
        }
    }
}
