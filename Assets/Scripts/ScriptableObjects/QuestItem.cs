﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class QuestItem : ScriptableObject {

    public Sprite sprite;
    public string nom;
    public string description;
    public int maxAmount;
    public int currentAmount;
}
