﻿using UnityEngine;

[CreateAssetMenu]
public class Weapon : ScriptableObject
{
    public Sprite sprite;
    public string itemName;
    public float weaponDamage;
    public float cooldown;
    public float projectileSpeed;
}
