﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NiveauForet : MonoBehaviour {
    [SerializeField] TalkWithContremaitre contremaitre;
    [SerializeField] TalkWithForgeron forgeron;
	[SerializeField] TalkWithApothicaire apothicaire;
    PlayerController joueur;

    public Sprite machineBroken;

    bool cutsceneDefeatMachineIsPlaying = false;
    int dialogStep = 0;

    // Use this for initialization
    void Start () {
        joueur = GameObject.Find("Protagoniste").GetComponent<PlayerController>();
        joueur.contremaitre = contremaitre;
        joueur.forgeron = forgeron;
		joueur.apothicaire = apothicaire;
        dialogStep = 0;
    }
	void Init()
    {
        GameObject go = GameObject.Find("Contremaitre");
        GameObject go1 = GameObject.Find("Forgeron");
		GameObject go2 = GameObject.Find("Apothicaire");
    }
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Interagir") || Input.GetButtonDown("Melee"))
        {
            if (cutsceneDefeatMachineIsPlaying == true) StartCoroutine(CutsceneDefeatBoss()); 
        }
    }

    public IEnumerator CutsceneDefeatBoss()
    {
        cutsceneDefeatMachineIsPlaying = true;
        PlayerController.freezeMovement(20000);

        if(dialogStep == 0)
        {
            DeathScreen.FadeIn = true;
            yield return new WaitForSeconds(1f);
            DeathScreen.FadeIn = false;
            GameObject.Find("MachinePrefab").transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = machineBroken;
            GameObject.Find("MachinePrefab").transform.GetChild(0).transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            GameObject.Find("MachinePrefab").transform.GetChild(0).transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
        }

        print(dialogStep);

        GameObject.Find("Contremaitre").transform.position = GameObject.Find("PositionContremaître").transform.position;
        GameObject.Find("Ouvrier").transform.position = GameObject.Find("PositionOuvrier").transform.position;
        GameObject.Find("Ouvrier (1)").transform.position = GameObject.Find("PositionOuvrier1").transform.position;
        GameObject.Find("Ouvrier (2)").transform.position = GameObject.Find("PositionOuvrier2").transform.position;
        GameObject.Find("Protagoniste").transform.position = GameObject.Find("PositionPlayer").transform.position;
        switch (dialogStep)
        {
            case 0:
                string dialogue = "Quelle horreur, cette machine ! Heureusement que vous l'avez arrêtée par la force..." +
                "On va s'occuper de la démonter avec les gars. On aura juste besoin de deux de vos cellules d'énergie pour faire sauter son... sa coque externe, là.";
                GameObject.Find("DialogBox").GetComponent<Text>().text = dialogue;
                GameObject.Find("Dialogues").GetComponent<canvasTestScript>().canvasGroup.alpha = 1;
                dialogStep++;
                break;
            case 1:
                dialogue = "Quelle horreur, cette machine ! Heureusement que vous l'avez arrêtée par la force..." +
                "On va s'occuper de la démonter avec les gars. On aura juste besoin de deux de vos cellules d'énergie pour faire sauter son... sa coque externe, là.";
                GameObject.Find("DialogBox").GetComponent<Text>().text = dialogue;
                GameObject.Find("Dialogues").GetComponent<canvasTestScript>().canvasGroup.alpha = 1;
                dialogStep++;
                break;
            case 2:
                dialogue = "Ah oui ! Y'a aussi le vieux -- euh, enfin, l'apothicaire qui veut vous voir. Il a quelque chose pour vous, qu'il dit."
                + "Si vous avez pas de chance, ce sera une de ses soupes bizarres. La dernière fois, y'avait un crapaud vivant et des ronces crues dedans...";
                GameObject.Find("DialogBox").GetComponent<Text>().text = dialogue;
                GameObject.Find("Dialogues").GetComponent<canvasTestScript>().canvasGroup.alpha = 1;
                dialogStep++;
                break;
            case 3:
                GameObject.Find("Dialogues").GetComponent<canvasTestScript>().canvasGroup.alpha = 0;
                cutsceneDefeatMachineIsPlaying = false;
                PlayerController.freezeMovement(0);

                AvancementQuete.responsable = true;
                AvancementQuete.QueteTerminee = true;
                //SceneManager.LoadScene("Village");
                break;
        }
    }
}
