﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PNJController : MonoBehaviour
{

    [SerializeField]
    private bool conversation = false;
    public static string dialogueToDisplay;
    [SerializeField]
    private GameObject textObject;
    [SerializeField]
    private CanvasGroup canvas;
    public static Text textZone;
    public AudioSource HuDBoîteDialogue;
    public AudioClip[] HuD;

    public Sprite tetePNJ;

    // Use this for initialization
    public void Start()
    {
    }

    // Update is called once per frame
    public void Update()
    {
        //Normalement ça devrait être dans Start(), mais apparemment ça s'initialise pas assez vite
        //Du coup pour l'instant j'y vais bourrin

        if (Input.GetButtonDown("Interagir") && conversation == true)
        {
            canvas.alpha = 1;
            textZone.text = dialogueToDisplay;
            SoundManager.PlayRandomOneShot(HuDBoîteDialogue, HuD, 0.5f, 0.6f, 0.9f, 1.1f, 1f);
        }

        //if(gameObject.name == "Ouvrier (1)") print(conversation);
    }

    //Quand le joueur entre dans la zone de discussion du PNJ, on récupère l'interface dialogues (qui est dans le prefab Protagoniste)
    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Protagoniste")
        {
            GameObject.Find("Protagoniste").GetComponent<PlayerController>().freezeActions();
            conversation = true;
            textObject = GameObject.Find("DialogBox");
            canvas = GameObject.Find("Dialogues").GetComponent<canvasTestScript>().canvasGroup;
            textZone = textObject.GetComponent<Text>();
        }
    }

    //Si le joueur s'éloigne du PNJ, on coupe le dialogue automatiquement
    public void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name == "Protagoniste")
        {
            conversation = false;
            canvas.alpha = 0;
            GameObject.Find("Protagoniste").GetComponent<PlayerController>().freezeActions();
        }
    }

    //Fonction appelée par les fonctions héritées pour définir le dialogue à afficher à l'écran
    public void setDialogue(string DialoguePNJ)
    {
        dialogueToDisplay = DialoguePNJ;
    }

    public void displayDialogue(string dialogue, CanvasGroup canvas)
    {
        canvas.alpha = 1;
        GameObject.Find("DialogBox").GetComponent<Text>().text = dialogue;
        GameObject.Find("TeteInterlocuteur").GetComponent<Image>().sprite = tetePNJ;
        SoundManager.PlayRandomOneShot(HuDBoîteDialogue, HuD, 0.5f, 0.6f, 0.9f, 1.1f, 1f);
    }
}