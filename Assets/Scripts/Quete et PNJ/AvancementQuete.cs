﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvancementQuete : MonoBehaviour {
    bool Checked = false;
    bool Checked2 = false;
    bool Checked3 = false;
    bool Checked4 = false;
    bool Checked5 = false;
    bool Checked6 = false;
    bool Checked7 = false;
    bool Checked8 = false;
    bool Checked9 = false;
	bool Checked998 = false;
    bool Checked999 = false;

    [SerializeField]
    public static bool findFaitCm = false;

    public static bool QueteEnCours = false;
    public static bool QueteTerminee = false;

    public static bool Ouvrier1 = false;
    public static bool Ouvrier2 = false;
    public static bool LoreApothicaire = false;
    public static bool Ouvrier3 = false;
    public static bool NeedHelpBag = false;
    public static bool SacRamasse = false;

	public static bool LevierRepare = false;
    public static bool Forgeron = false;
    public static bool NeedHelpLevierBroken = false;
    public static bool priseDeConscienceDuJunkParLeForgeron = false;
    public static bool GoForgeronJunkTrouve = false;
    public static bool GoForgeronPeterTaCeinture = false;
    public static bool Outil = false;

    public static int JunkTrouve = 0;
    public static bool WaitingForBois1 = false;
    public static bool BoisCoupe1 = false;
    public static bool WaitingForBois2 = false;
    public static bool BoisCoupe2 = false;
    public static bool WaitingForBois3 = false;
    public static bool BoisCoupe3 = false;
    public static bool AlternativeImpossible = false;
    public static bool Consumeriste = false;

    public static bool PriseDeConscience = false;
    public static bool AlternativeObligatoire = false;
    public static bool ContremaitreIntrigue = false;
    public static bool EnRoute = false;
	public static bool DefenseStart = false;
	public static bool GGWP = false;
	public static bool responsable = false;

    public static bool apothicaireGivesPotions = false;

    public int bossMaxHealth;
    // Use this for initialization
    void Start() {
        bossMaxHealth = 100;
    }

    // Update is called once per frame
    void Update() {

        if (JunkTrouve >= 2) GoForgeronJunkTrouve = true;
       
        if (Checked == false)
        {
            Check();
        }

        if (Checked2 == false)
        {
            Check2();
        }

        if (Checked3 == false)
        {
            Check3();
        }

        if (Checked4 == false)
        {
            Check4();
        }

        if (Checked5 == false)
        {
            Check5();
        }

        if (Checked6 == false)
        {
            Check6();
        }

        if (Checked7 == false)
        {
            Check7();
        }

        if (Checked8 == false)
        {
            Check8();
        }

        if (Checked9 == false)
        {
            Check9();
        }
		
		if (Checked998 ==false)
		{
			Check998();
		}	

        if (Checked999 == false)
        {
            Check999();
        }
    }

    void Check() {
        if (QueteEnCours == true)
            {
                print("Nouvelle quête : Foret Polluée"); //-------BESOIN FEEDBACK------- 
                Checked = true;
        }
    }

    void Check2 () {
        if (Ouvrier1 == true)
        {
            print("OUVRIER ROUGE OK");
            Checked2 = true;
        }
    }

    void Check3()
    {
        if (Ouvrier2 == true)
        {
            print("OUVRIER VERT OK");
            Checked3 = true;
        }
    }

    void Check4()
    {
        if (Ouvrier3 == true)
        {
            print("OUVRIER BLEU OK");
            Checked4 = true;
        }
    }

    void Check5()
    {
        if (Ouvrier1 == true && Ouvrier2 == true && Ouvrier3 == true)
        {
            print("Tout les ouvriers ont été recruté.");
            Checked5 = true;
        }
    }

    void Check6()
    {
        if (Forgeron == true)
        {
            print("FORGERON OK");
            Checked6 = true;
        }
    }

    void Check7()
    {
        if (BoisCoupe1 == true)
        {
            Checked7 = true;
        }
    }

    void Check8()
    {
        if (BoisCoupe2 == true)
        {
            Checked8 = true;
        }
    }

    void Check9()
    {
        if (BoisCoupe3 == true)
        {
            //Consumeriste = true;
            Checked9 = true;
        }
    }
	
	void Check998()
	{
		if(GGWP == true)
		{
			print("Défense Reussi ! Retournez voir le contremaitre"); //------BESOIN FEEDBACK----------
			Checked998 = true;
		}
	}
	
	void Check999()
    {
        if (QueteTerminee == true)
        {
            print("Quête terminée !"); //----------BESOIN FEEDBACK-------
            if(Consumeriste)
			{
                GameObject.Find("Protagoniste").GetComponent<HealthScript>().maxHealth = 4;      //INSERER ICI LA VARIABLE DE LA CEINTURE DE PROTECTION QUI RAJOUTE DE LA VIE ET DE L ENERGIE.
                GameObject.Find("Protagoniste").GetComponent<HealthScript>().health = 4;
                GameObject.Find("Protagoniste").GetComponent<EnergyScript>().maxEnergy += 5;
                bossMaxHealth += 25;            
			}
			if(responsable)
			{
                bossMaxHealth -= 25;
                apothicaireGivesPotions = true;
                GameObject.Find("Protagoniste").GetComponent<EnergyScript>().maxEnergy -= 2;
                GameObject.Find("Protagoniste").GetComponent<EnergyScript>().energy = GameObject.Find("Protagoniste").GetComponent<EnergyScript>().maxEnergy;
            }
            Checked999 = true;
			QueteEnCours = false;
        }
    }
}
