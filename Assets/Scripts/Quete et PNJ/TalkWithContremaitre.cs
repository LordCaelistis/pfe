﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkWithContremaitre : PNJController {

    [SerializeField] GameObject boutonQueteAccepte;
    [SerializeField] GameObject boutonQueteRefuse;
    [SerializeField] GameObject boutonBois1Oui;
    [SerializeField] GameObject boutonBois1Non;
    [SerializeField] GameObject boutonJunkTrouve;
    [SerializeField] GameObject boutonBois2Oui;
    [SerializeField] GameObject boutonBois3Oui;
    [SerializeField] GameObject boutonForetEnDanger;
    GameObject cm;
    // Use this for initialization
    new void Start() {
        base.Start();
        boutonQueteAccepte = GameObject.Find("Dialogues").transform.GetChild(2).gameObject;
        boutonQueteRefuse = GameObject.Find("Dialogues").transform.GetChild(3).gameObject;
        boutonBois1Oui = GameObject.Find("Dialogues").transform.GetChild(4).gameObject;
        boutonBois1Non = GameObject.Find("Dialogues").transform.GetChild(5).gameObject;
        boutonJunkTrouve = GameObject.Find("Dialogues").transform.GetChild(6).gameObject;
        boutonBois2Oui = GameObject.Find("Dialogues").transform.GetChild(7).gameObject;
        boutonBois3Oui = GameObject.Find("Dialogues").transform.GetChild(8).gameObject;
        boutonForetEnDanger = GameObject.Find("Dialogues").transform.GetChild(9).gameObject;
        if (AvancementQuete.findFaitCm==false) { 
            cm= GameObject.Find("Contremaitre");
            print("LE FIND DU CONTREMAITRE SE LANCE");
            AvancementQuete.findFaitCm = true;
        }

        print(AvancementQuete.findFaitCm);

        GameObject.Find("Dialogues").transform.GetChild(2).gameObject.SetActive(false);
        boutonQueteRefuse.SetActive(false);
        boutonBois1Oui.SetActive(false);
        boutonBois1Non.SetActive(false);
        boutonJunkTrouve.SetActive(false);
        boutonBois2Oui.SetActive(false);
        boutonBois3Oui.SetActive(false);
        boutonForetEnDanger.SetActive(false);
    }
    
    new void Update() {
        base.Update();
        
    }

    new void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.gameObject.name == "Protagoniste")
        {
			if(AvancementQuete.QueteTerminee == false) //SI ON A PAS FINI LA QUETE
			{
				if (AvancementQuete.QueteEnCours == false) //SI ON A PAS LA QUÊTE
				{
					setDialogue("Vous venez pour les réparations du village ? Les gars et moi avons beaucoup de mal à travailler, on manque de bois et personne n’arrive à aller "+
								"en chercher. J’ai envoyé Peter et Archie s’en occuper mais ils sont revenus les mains vides. On ne peut pas réparer le village sans bois..." +
                                "Votre aide pourrait être utile. Ca vous dirait de gagner votre pain sur un chantier ?");
					boutonQueteAccepte.SetActive(true);
					boutonQueteRefuse.SetActive(true);
				}
				else //SI ON A LA QUÊTE
				{
					if (AvancementQuete.ContremaitreIntrigue == true) //SOIT C'EST QUE LE CONTREMAITRE EST INTRIGUE
					{
						if (AvancementQuete.Outil == true) //SI ON EST ALLER CRAFTER L'OUTIL DU FORGERON 
						{
							if (AvancementQuete.GGWP ==true) //SI ON A REUSSI LA PHASE DE DEFENSE
							{
								setDialogue("Eh bien… Regardez ça, les plantes commencent à repousser,"+
								"je pense que les arbres feront la même chose. En attendant, on aurait dû mieux gérer la situation, c'est sûr... On avait clairement la tête" +
                                "dans le guidon");
								AvancementQuete.responsable=true;
								AvancementQuete.QueteTerminee=true;
							}
							else //NOUS PARTONS POUR LA FORET POUR LA PURIFIER VERS LA PHASE DE DEFENSE.
							{
								setDialogue("Parfait, avec ça on aura fini dans pas longtemps. LES GARS ! On détruit ça, et on rentre pour dîner !");
								AvancementQuete.EnRoute = true;
							}
						}
						else //LE CONTREMAITRE SE REND COMPTE QUE C'EST LA MERDE ET NOUS ENVOI CRAFTER L'OUTIL AU FORGERON
						{
							setDialogue("COMMENT CETTE SALETÉ DE MACHINE PEUT ÊTRE ENCORE DEBOUT ?! On va pas laisser cette chose détruire davantage tout ce qu’on possède ! "+
							"Il est temps de se mettre au travail, aller les gars ! On va là-bas et on règle ça vite fait, bien fait ! Quant à vous, attendez une minute, "+
							"on va s’assurer d’avoir assez de matériel pour la détruire, allez voir le forgeron, ça fait des mois qu’il doit me fabriquer un marteau de chantier, "+
							"rapportez le moi et on pourra s’y coller. On se retrouve devant la machine.");
							AvancementQuete.GoForgeronPeterTaCeinture = true;
						}
					}
					else //SOIT IL EST PAS ENCORE INTRIGUE ET MONSIEUR S'IMPATIENTE JUSTE
					{
						if (AvancementQuete.WaitingForBois1 == true) //SI IL T A DEMANDER 3 BUCHES DE CHENE
						{
							setDialogue("Vous avez mes 6 bûches ? J'ai besoin de ce bois.");
							if (AvancementQuete.BoisCoupe1 == true)
							{
								boutonBois1Oui.SetActive(true);
							}
							else
							{
								boutonBois1Non.SetActive(true);
							}
						}

						if (AvancementQuete.WaitingForBois2 == true) //SI IL T A DEMANDER 13 BUCHES DE SAPIN
						{ 
							setDialogue("Vous avez mes 7 bûches ? J'ai besoin de ce bois.");
							if (AvancementQuete.BoisCoupe2 == true)
							{
								boutonBois2Oui.SetActive(true);
							}
							else
							{
								boutonBois1Non.SetActive(true);
							}
						}

						if (AvancementQuete.WaitingForBois3 == true) //SI IL T A DEMANDER 25 BUCHES DE EBENE
						{
							setDialogue("Vous avez mes 12 bûches ? J'ai besoin de ce bois.");
							if (AvancementQuete.BoisCoupe3 == true)
							{
								boutonBois3Oui.SetActive(true);
							}
							else
							{
								boutonBois1Non.SetActive(true);
							}
						}
					}
				}
			}
			else // SI TU AS FINI LA QUETE
			{
				setDialogue("C'était sympa, la forêt. L'ombre, les oiseaux, tout ça... Mais bon, on arrête pas le progrès, pas vrai ?");
			}	
        }
    }

    new void OnTriggerExit2D(Collider2D col)
    {
        base.OnTriggerExit2D(col);
        if (col.gameObject.name == "Protagoniste")
        {
            boutonQueteAccepte.SetActive(false);
            boutonQueteRefuse.SetActive(false);
            boutonBois1Oui.SetActive(false);
            boutonBois1Non.SetActive(false);
            boutonJunkTrouve.SetActive(false);
            boutonBois2Oui.SetActive(false);
            boutonBois3Oui.SetActive(false);
            boutonForetEnDanger.SetActive(false);
        }  
    }

    public void QueteAcceptee()
    {
        setDialogue("Enfin quelqu’un avec un peu d'enthousiasme pour travailler. On s’occupe de réparer la scierie, pendant ce temps allez dans la forêt couper du bois, "+
					"6 bûches devraient suffire. Une fois la scierie remontée on se remettra au travail. Faites pas attention à la substance étrange sur le chemin, "+
					"elle se répand ici depuis longtemps... On a fini par s’y habituer.");
        textZone.text = dialogueToDisplay;
        boutonQueteAccepte.SetActive(false);
        boutonQueteRefuse.SetActive(false);
        AvancementQuete.QueteEnCours = true;
        AvancementQuete.WaitingForBois1 = true;
    }
    
    public void QueteRefusee()
    {
        setDialogue("Comme vous voulez, si personne ne va chercher de bois on ne pourra pas réparer les habitations. Si jamais vous changez d’avis...");
        textZone.text = dialogueToDisplay;
        boutonQueteAccepte.SetActive(false);
        boutonQueteRefuse.SetActive(false);
    }

    public void ImpatienceOui() // SI TU LUI AS RAMENER 3 BUCHE DE CHENE, IL TE DEMANDE DES SAPINS
    {
        setDialogue("Parfait, allez les gars on se met au tra… Bon sang Peter ! Fais attention ! Tu viens de nous faire perdre un lot de planches ! "+
					"Bon… Je suppose que maintenant on va encore avoir besoin de bois. On aura besoin de 7 bûches cette fois. "+
					"Hésitez pas à vous enfoncer plus loin dans la forêt, il doit rester quelques arbres dans les environs.");
        GameObject.Find("Inventaire").GetComponent<InventaireItems>().GiveBûches(6);
        textZone.text = dialogueToDisplay;
        boutonBois1Oui.SetActive(false);
        AvancementQuete.WaitingForBois1 = false;
        AvancementQuete.WaitingForBois2 = true;
        if (AvancementQuete.JunkTrouve != 0)
        {
            boutonJunkTrouve.SetActive(true);
        }
    }

    public void ImpatienceNon() //SI TU AS PAS CE QU IL T'A DEMANDE
    {
        setDialogue("Hé bien qu’est ce que vous attendez ? Bougez vous un peu !");
        textZone.text = dialogueToDisplay;
        boutonBois1Non.SetActive(false);
        if (AvancementQuete.JunkTrouve != 0)
        {
            boutonJunkTrouve.SetActive(true);
        }
    }

    public void DechetTrouve()
    {
        setDialogue("Ces p’tites babioles ? Oui on en trouve souvent par ici, le forgeron les recycle pour en faire des outils ou des objets. Vous devriez lui amener, mais n’oubliez pas notre bois hein ! ");
        textZone.text = dialogueToDisplay;
        boutonBois1Non.SetActive(false);
        boutonJunkTrouve.SetActive(false);
        AvancementQuete.GoForgeronJunkTrouve = true;
    }

    public void Impatience2Oui() //SI TU LUI AS RAMENER 5 BUCHES DE SAPINS, IL TE DEMANDE DES EBENES 
    {
        GameObject.Find("Inventaire").GetComponent<InventaireItems>().GiveBûches(7);
        setDialogue("Bien, mais j’ai une mauvaise nouvelle pour vous, on vient de me dire qu’un bâtiment s’est effondré dans la carrière. "+
					"Résultat, on doit livrer suffisamment de bois pour le réparer…  Vous voulez bien aller en chercher une dernière fois ? Cette fois on aura besoin de 12 bûches, "+
					"de quoi maintenir les fondations. Un dernier effort et on pourra rentrer se reposer !");
        textZone.text = dialogueToDisplay;
        boutonBois2Oui.SetActive(false);
        AvancementQuete.WaitingForBois2 = false;
        AvancementQuete.WaitingForBois3 = true;
        if (AvancementQuete.JunkTrouve != 0)
        {
            boutonJunkTrouve.SetActive(true);
        }
    }

    public void Impatience3Oui() //SI TU LUI AS RAMENER 7 BUCHE D EBENES, TU A RASER LA FORET, FDP :)
    {
        GameObject.Find("Inventaire").GetComponent<InventaireItems>().GiveBûches(12);
        setDialogue("Enfin, cette fois c’est la bonne ! Allez les gars, on se met au travail ! On a tout le nécessaire pour les réparations du village. "+
					"On s’occupe de tout livrer, votre travail s’arrête là. Tenez, c’est pour vos efforts, "+
					"sachez qu’ici on tient à ceux qui travaillent dur, j’me suis permis d’ajouter un renfort en bois à votre manteau, avec ça vous pourrez encaisser plus de coups.");
        textZone.text = dialogueToDisplay;
        boutonBois3Oui.SetActive(false);
        AvancementQuete.WaitingForBois3 = false;
        AvancementQuete.Consumeriste = true;
        AvancementQuete.QueteTerminee = true;
        
    }

    public void ForetEnDanger() //SI TU LUI DIS QUE TU AS DECOUVERT UNE MACHINE QUI RUINE SA FORET
	{
		boutonJunkTrouve.SetActive(false);
		setDialogue(".............."); //*il part*
		textZone.text = dialogueToDisplay;
			//cm.SetActive(false);    
		AvancementQuete.ContremaitreIntrigue = true;
		AvancementQuete.AlternativeObligatoire=true;
	}
}