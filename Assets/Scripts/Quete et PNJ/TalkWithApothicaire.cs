﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkWithApothicaire : PNJController
{
	[SerializeField] GameObject boutonLoreNon;
	[SerializeField] GameObject boutonLore1;
    [SerializeField] GameObject boutonLore2;
    [SerializeField] GameObject boutonLore3;

    // Use this for initialization
    new void Start()
    {
        base.Start();
		boutonLoreNon = GameObject.Find("Dialogues").transform.GetChild(13).gameObject;
        boutonLore1 = GameObject.Find("Dialogues").transform.GetChild(14).gameObject;
        boutonLore2 = GameObject.Find("Dialogues").transform.GetChild(15).gameObject;
        boutonLore3 = GameObject.Find("Dialogues").transform.GetChild(16).gameObject;
		
		boutonLoreNon.SetActive(false);
        boutonLore1.SetActive(false);
        boutonLore2.SetActive(false);
        boutonLore3.SetActive(false);
    }

    new void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.gameObject.name == "Protagoniste")
        {
            if (AvancementQuete.LoreApothicaire == true && AvancementQuete.apothicaireGivesPotions == false)
            {
                setDialogue("Bonjour Jeune fille. Que puis-je pour vous ? \nVous voulez en savoir plus sur cet endroit ?"); 
                boutonLoreNon.SetActive(true);
				boutonLore1.SetActive(true);        
            }
            else if (AvancementQuete.LoreApothicaire == true && AvancementQuete.responsable == true)
            {
                setDialogue("Vous avez rendu un grand service au village en détruisant cette machine infernale..." +
                    "Nous en percevons déjà les bienfaits. Les plants d'herbes médicinales sont déjà revitalisés." +
                    "Ce n'est qu'une maigre récompense... mais voici des remèdes pour guérir vos blessures en cas de coup dur. ");
                InventaireItems.GetPotions();
                //boutonLoreNon.SetActive(true);
                //boutonLore1.SetActive(true);
            }
            else
            {
                /*setDialogue("Hummm, bonjour. Vous vous réveillez enfin jeune fille ? Je suis l'apothicaire du village et je vous ai soigné. " +
                    "Ce qui s'est passé ? Il semblerais que vous vous soyez faite soulever comme jaja ma petite. Quel idée d'affronter cette chose... " +
                    "Si vous voulez y retourner, l'ascenseur à gauche de ma maison vous y conduira. Mais vous feriez mieux de parcourir notre" +
                    " village avant d'y retourner. Si vous voulez vous rendre vous utile, le contremaitre à l'est du village à besoin d'aide.");*/
				setDialogue("Je vois que mes soins ont été efficaces. J’ai puisé dans mes réserves de plantes médicinales pour vous maintenir en vie... "+
				"Vous avez de la chance, d’habitude personne n’arrive à sortir en un seul morceau. "+ 
                "Si vous souhaitez vous rendre utile au village, le contremaître recherche de la main d’oeuvre dans la forêt, à l'est. "+
				"Revenez me voir si vous souhaitez en savoir plus sur cet endroit !");	
                AvancementQuete.LoreApothicaire = true;
            }
        }
    }
	
	    new void OnTriggerExit2D(Collider2D col)
    {
        base.OnTriggerExit2D(col);
        if (col.gameObject.name == "Protagoniste")
        {
			boutonLoreNon.SetActive(false);            
			boutonLore1.SetActive(false);
            boutonLore2.SetActive(false);
            boutonLore3.SetActive(false);
        }  
    }
	
	public void LoreNon() //SI TU LUI NON PAS DE LORE
	{
		boutonLore1.SetActive(false);
		boutonLoreNon.SetActive(false);
		setDialogue("Dans ce cas je ne vous retiens pas plus longtemps, revenez me voir quand vous le souhaitez.");
		textZone.text = dialogueToDisplay;
	}
	
	public void Lore1() //SI TU LUI DIS OUI JE VEUX DU LORE
	{
		boutonLore1.SetActive(false);
		//boutonLoreNon.SetActive(false);
		setDialogue("La création de notre village remonte à des centaines d’années. A cette époque il n’y avait que quelques maisons et quelques parcelles de champs. "+
					"Les habitants ont utilisé une technologie ancienne pour le faire grandir et créer une carrière, une scierie, des fermes et un tas d’autres structures. "+
					"Ils les ont ensuite transmis aux générations suivantes jusqu’à nos jours. \nVous voulez entendre la suite ?");
		textZone.text = dialogueToDisplay;
		boutonLore2.SetActive(true);
	}
	
	public void Lore2() //SI TU LUI DIS OUI JE VEUX ENCORE DU LORE
	{
		boutonLore2.SetActive(false);
		//boutonLoreNon.SetActive(false);
		setDialogue("Au fur et à mesure que le temps passait, les ressources se sont amoindries, nos ancêtres n’avaient plus les moyens suffisants pour faire fonctionner leurs "+
					"machines et les ont abandonné dans un sous-sol. On ne sait pas ce qu’elles sont devenues mais tous ceux qui ont tenté d’aller les récupérer sont morts. "+
					"Vous avez eu de la chance que l’on vous trouve là-dessous. \nVous voulez entendre la suite ?");
		textZone.text = dialogueToDisplay;
		boutonLore3.SetActive(true);
	}
	
	public void Lore3() //SI TU LUI DIS OUI JE VEUX TOUJOURS PLUS DE LORE
	{
		boutonLore3.SetActive(false);
		boutonLoreNon.SetActive(false);
		setDialogue("La situation s’est empirée récemment... Nous avons vécu de nombreuses catastrophes naturelles et le village est en piteux état. Nos ouvriers font le maximum,"+
					"mais nous avons besoin d’énormément de ressources pour vivre et les faire fonctionner. "+
					"Je pense que vous en savez bien assez maintenant, revenez me voir quand vous le souhaitez et faites attention à vous.");
		textZone.text = dialogueToDisplay;
	}
}