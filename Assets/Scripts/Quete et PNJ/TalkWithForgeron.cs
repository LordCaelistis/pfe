﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkWithForgeron : PNJController {
    [SerializeField] GameObject boutonForgeron1;
    [SerializeField] GameObject boutonForgeron2;
	[SerializeField] GameObject boutonForgeron3;
    public static bool Minerais = false;

    // Use this for initialization

    new void Start()
    {
        base.Start();
        boutonForgeron1 = GameObject.Find("Dialogues").transform.GetChild(10).gameObject;
        boutonForgeron2 = GameObject.Find("Dialogues").transform.GetChild(11).gameObject;
		boutonForgeron3 = GameObject.Find("Dialogues").transform.GetChild(12).gameObject;

        boutonForgeron1.SetActive(false);
        boutonForgeron2.SetActive(false);
		boutonForgeron3.SetActive(false);
    }

    new void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.gameObject.name == "Protagoniste")
        {
            if (AvancementQuete.QueteEnCours == true)
            {
				if (AvancementQuete.GoForgeronJunkTrouve == false) //SI TU A PAS TROUVE LE JUNK
				{
					setDialogue("Bienvenue dans ma boutique. Je forge des outils, des armes et du matériel, sinon je recycle vos babioles et tout ce que vous pouvez trouver."); 
				}
				else //SI A TROUVE LE JUNK
				{
					if (AvancementQuete.JunkTrouve == 1)
					{
						setDialogue("Qu'est-ce que c'est que cela, vous dites ? Des bouts d'une machine, " +
                        "mais d'une technologie qui me dépasse. Je pourrais facilement le reforger en autre chose si j'en avais au moins un de plus...");
					}
					else
					{
                        setDialogue("Vous avez trouvé deux de ces bricoles ? Attendez un instant... " +
                            "Et voilà ! Une pièce mécanique articulée. Avec ça, vous devriez pouvoir réparer quelques objets sur votre chemin. " +
                        "On ne sait jamais, ça pourrait être utile...");
                        AvancementQuete.priseDeConscienceDuJunkParLeForgeron = true;
                        AvancementQuete.LevierRepare = true;
                        AvancementQuete.JunkTrouve = 0;
                        GameObject.Find("NombreJunk").GetComponent<Text>().text = AvancementQuete.JunkTrouve.ToString();

                    }
				}
            }
            else
            {
                setDialogue("Bienvenue dans ma boutique. Je forge des outils, des armes et du matériel, sinon je recycle vos babioles et tout ce que vous pouvez trouver.");
            }
        }
    }

    new void OnTriggerExit2D(Collider2D col)
    {
        base.OnTriggerExit2D(col);
        if (col.gameObject.name == "Protagoniste")
        {
            boutonForgeron1.SetActive(false);
            boutonForgeron2.SetActive(false);
        }
    }

	    public void ReparerLevier()
    {
        setDialogue("Et voila, 2 engrenages tout neuf !");
        textZone.text = dialogueToDisplay;
        boutonForgeron3.SetActive(false);
        AvancementQuete.LevierRepare = true;
		AvancementQuete.NeedHelpLevierBroken=false;
    }
	
    public void ForgerOutil()
    {
        setDialogue("Et voila, attention il est encore relativement instable. Bonne chance et dites au contremaître qu’il me doit toujours de l’argent pour son casque !");
        textZone.text = dialogueToDisplay;
        boutonForgeron1.SetActive(false);
        boutonForgeron2.SetActive(false);
        GameObject.Find("Protagoniste").GetComponent<EnergyScript>().maxEnergy -= 2;
        AvancementQuete.Outil = true;
    }

    public void RefuserOutil()
    {
        setDialogue("C’est un sacrifice important, mais il permettra de sauver des vies. Laissez moi quelques heures pour terminer. "); //fondu au noir
        textZone.text = dialogueToDisplay;
        boutonForgeron1.SetActive(false);
        boutonForgeron2.SetActive(false);
    }
}