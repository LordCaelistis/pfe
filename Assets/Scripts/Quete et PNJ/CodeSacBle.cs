﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeSacBle : MonoBehaviour {
    bool canTakeBag = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //Si j'appuit sur Interagir à proximité du sac et qu'on m'a dit de le faire et que j'en transporte pas deja un.
        if (Input.GetButtonDown("Interagir") && canTakeBag == true && AvancementQuete.NeedHelpBag == true && AvancementQuete.SacRamasse==false)
        {
            AvancementQuete.SacRamasse = true;
            Destroy(gameObject);
            print("Sac ramassé");
        }
    }

    void OnTriggerEnter2D(Collider2D col)
        {
            canTakeBag = true;
        }

    void OnTriggerExit2D(Collider2D col)
    {
        canTakeBag = false;
    }
}