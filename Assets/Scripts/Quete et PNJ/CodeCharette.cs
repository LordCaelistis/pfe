﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeCharette : MonoBehaviour {
    bool canUseCharette = false;
    int nbSacDepose = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //Si j'appuit sur Interagir à proximité de la charette et que j'ai un sac de blé
        if (Input.GetButtonDown("Interagir") && canUseCharette == true && AvancementQuete.SacRamasse == true)
        {
            AvancementQuete.SacRamasse = false;
            print("Sac déposé");
            nbSacDepose += 1;
        }

        if (nbSacDepose == 3)
        {
            TalkWithOuvrierBleu.Provision = true;
            print("Tout les sacs de blé ont été déposé");
            nbSacDepose = 0;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        canUseCharette = true;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        canUseCharette = false;
    }
}