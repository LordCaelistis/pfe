﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeLevierBroken : MonoBehaviour {
	
	public AudioSource SourceSon;
    public GameObject tapisRoulant;
    public AudioClip[] InterLevier;
	
	public SpriteRenderer spriteLevier;
    bool ProxLevierBroken = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        print(AvancementQuete.JunkTrouve);
        print(AvancementQuete.priseDeConscienceDuJunkParLeForgeron);

        //Si j'appuit sur Interagir à proximité de la sonde alors que j'ai la quete.
        if (Input.GetButtonDown("Interagir") && ProxLevierBroken == true && AvancementQuete.QueteEnCours == true)
        {
            print("Le mécanisme de ce levier semble endommagé, mais il est peu être réparable..."); //---------BESOIN FEEDBACK-----------

            if (AvancementQuete.LevierRepare)
			{
                //AvancementQuete.NeedHelpLevierBroken = true;
                spriteLevier.color = Color.green;
                print("Le levier semble reparé"); //---------BESOIN FEEDBACK-----------
                SoundManager.PlayRandomOneShot(SourceSon, InterLevier, 0.2f, 0.3f, 0.9f, 1.1f, 1f); //ICI J AI MIS LE CODE DU LEVIER, PAS SUR QUE TOUT MARCHE A TESTER 
                GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
                tapisRoulant.GetComponent<TapisRoulant>().changerDirection = !tapisRoulant.GetComponent<TapisRoulant>().changerDirection;
            }
			else
			{
                print("Le mécanisme de ce levier semble endommagé, mais il est peu être réparable..."); //---------BESOIN FEEDBACK-----------
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        ProxLevierBroken = true;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        ProxLevierBroken = false;
    }
}