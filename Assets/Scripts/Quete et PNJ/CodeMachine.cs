﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeMachine : EnemyController {
    bool ProxMachine = false;
    float duree_defense = 60;
    public float cooldown_defense = 0;

    // Desired duration of the shake effect
    private float shakeDuration = 0f;
    // A measure of magnitude for the shake. Tweak based on your preference
    public float shakeMagnitude = 0.7f;
    // A measure of how quickly the shake effect should evaporate
    public float shakeFadeOut = 1.0f; 

    new void Start()
    {
        base.Start();
    }
	
	// Update is called once per frame
	new void Update () {

        if (Input.GetKeyDown("m"))
        {
            health = 5;
        }

        if (shakeDuration > 0)
        {
            transform.localPosition = transform.position + Random.insideUnitSphere * shakeMagnitude;
            shakeDuration -= Time.deltaTime * shakeFadeOut;
        }
        else
        {
            shakeDuration = 0f;
        }

        if(health == 0) AvancementQuete.GGWP = true;

        //Si j'appuis sur melee à proximité de la machine alors que j'ai la quete.
        if (Input.GetButtonDown("Melee") && ProxMachine == true || Input.GetButtonDown("Interagir") && ProxMachine == true)
        {
            AvancementQuete.PriseDeConscience = true;
            print("Ce truc contamine  la forêt ! Je devrai prevenir les autres."); //--------BESOIN FEEDBACK----------
            AvancementQuete.NeedHelpLevierBroken = false;
        }
		
		/*if (Input.GetButtonDown("Interagir") && ProxMachine == true && AvancementQuete.EnRoute == true && AvancementQuete.DefenseStart == false)
        {
            AvancementQuete.DefenseStart = true;
            print("DES MONSTRES ARRIVENT, PROTEGEZ NOUS LE TEMPS QU'ON DETUISE CETTE ABOMINATION !");
        }*/
    }

    new void TakeDamage(float damageTaken)
    {
        shakeDuration = 0.2f;
        base.TakeDamage(damageTaken);
        print("coucou");
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        ProxMachine = true;
        if (col.gameObject.name == "TirEnnemi(Clone") Destroy(col.gameObject);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        ProxMachine = false;
    }
}