﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeJunk : MonoBehaviour {
    bool canTakeJunk = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Si j'appuit sur Interagir à proximité du sac et qu'on m'a dit de le faire
        if (Input.GetButtonDown("Interagir") && canTakeJunk == true && AvancementQuete.QueteEnCours == true)
        {
            GameObject.Find("Inventaire").GetComponent<InventaireItems>().GetJunk();
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        canTakeJunk = true;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        canTakeJunk = false;
    }
}