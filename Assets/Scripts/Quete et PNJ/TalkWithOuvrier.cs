﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkWithOuvrier : PNJController
{

    // Use this for initialization
    new void Start()
    {
        base.Start();

    }

    new void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.gameObject.name == "Protagoniste")
        {
            if (AvancementQuete.QueteEnCours == true)
            {
                setDialogue("Il n'y a plus beaucoup de bois utilisable dans cette foret, bonne chance pour en trouver");
            }
            else
            {
                setDialogue("Bonjour, vous venez nous aider à couper du bois ? Le contremaitre est un peu plus loin, il vous en dira plus.");
                //potentiel bouton pour faire parler l'ouvrier en nous indiquant ou se situe le contremaitre mais bon pas giga utile pour le moment.
            }
        }
    }
}