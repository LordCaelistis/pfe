﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraControl : MonoBehaviour {

    public GameObject player;

    private float offset;
    private Scene currentScene;
    private GameObject cameraFocus;
    public static bool canMoveHorizontal = true;
    public static bool canMoveVertical = true;
    bool hitStun = false;

    private string PrevScene;

    public PostProcessingProfile postProcessBoss;
    public PostProcessingProfile postProcessVillage;
    public PostProcessingProfile postProcessForêt;

    // Desired duration of the shake effect
    private float shakeDuration = 0f;
    // A measure of magnitude for the shake. Tweak based on your preference
    public float shakeMagnitude = 0.7f;
    // A measure of how quickly the shake effect should evaporate
    public float shakeFadeOut = 1.0f;
    // The initial position of the GameObject
    Vector3 positionDébutShake;

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this.gameObject);
        currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "SceneBoss") cameraFocus = GameObject.Find("CameraFocus");
        player = GameObject.Find("Protagoniste");
    }

    private void OnLevelWasLoaded(int level)
    {
        canMoveHorizontal = true;
        canMoveVertical = true;

        currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "SceneBoss") cameraFocus = GameObject.Find("CameraFocus");
        if (currentScene.name == "SceneBoss" || currentScene.name == "CouloirBoss") GetComponent<PostProcessingBehaviour>().profile = postProcessBoss;
        if (currentScene.name == "Village" || currentScene.name == "Maison") GetComponent<PostProcessingBehaviour>().profile = postProcessVillage;
        if (currentScene.name == "Foret_Polluee_V2") GetComponent<PostProcessingBehaviour>().profile = postProcessForêt;

        /*if (PlayerPrefs.HasKey("SceneName")) PrevScene = PlayerController.PrevScene;
        if (SceneManager.GetActiveScene().name == "Foret_Polluee")
        {
            transform.position = new Vector3(-6.673989f, -2.097111f, -10);
        }

        if(SceneManager.GetActiveScene().name == "Village")
        {
            transform.position = new Vector3(58.53789f, -4.935112f, -9.01f);
        }*/
    }

    // Update is called once per frame
    void LateUpdate () {
        if (shakeDuration > 0)
        {
            transform.localPosition = positionDébutShake + Random.insideUnitSphere * shakeMagnitude;
            shakeDuration -= Time.deltaTime * shakeFadeOut;
            hitStun = true;
        }
        else
        {
            shakeDuration = 0f;
            hitStun = false;
        }

        //print(shakeDuration);

        if (currentScene.name != "SceneBoss" && currentScene.name != "Maison")
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            if (!hitStun)
            {
                if (canMoveHorizontal && canMoveVertical)
                {
                    transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
                    Camera.main.orthographicSize = 7f;
                }
                else if (!canMoveHorizontal && canMoveVertical)
                {
                    transform.position = new Vector3(transform.position.x, player.transform.position.y, -10);
                    Camera.main.orthographicSize = 7f;
                }
                else if (canMoveHorizontal && !canMoveVertical)
                {
                    transform.position = new Vector3(player.transform.position.x, transform.position.y, -10);
                    Camera.main.orthographicSize = 7f;
                }
                else
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, -10);
                }
            }
        }
        else if (currentScene.name == "SceneBoss")
        {
            if (canMoveHorizontal) transform.position = new Vector3(cameraFocus.transform.position.x, cameraFocus.transform.position.y, -10);
            Camera.main.orthographicSize = 8.5f;
        }
        else if (currentScene.name == "Maison")
        {
            if (canMoveHorizontal) transform.position = new Vector3(0, 0, -10);
            Camera.main.orthographicSize = 5.5f;
        }
    }

    public IEnumerator ShakeCamera(float timeToShake)
    {
        positionDébutShake = transform.position;
        shakeDuration = timeToShake;
        canMoveHorizontal = false;
        canMoveVertical = false;
        yield return new WaitForSeconds(shakeDuration);
        canMoveHorizontal = true;
        canMoveVertical = true;
    }
}

/*using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    public Vector3 playerPosition;

    private float offset;
    private Scene currentScene;
    public static bool canMoveHorizontal = true;
    public static bool canMoveVertical = true;

    private string PrevScene;

    private struct PointInSpace
    {
        public Vector3 Position;
        public float Time;
    }

    [Tooltip("The delay before the camera starts to follow the target")]
    [SerializeField]
    private float delay = 0.1f;

    [SerializeField]
    [Tooltip("The speed used in the lerp function when the camera follows the target")]
    private float speed = 5;

    ///<summary>
    /// Contains the positions of the target for the last X seconds
    ///</summary>
    private Queue<PointInSpace> pointsInSpace = new Queue<PointInSpace>();

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this.gameObject);
        //offset = transform.position.x - player.transform.position.x;
        currentScene = SceneManager.GetActiveScene();
    }

    private void OnLevelWasLoaded(int level)
    {
        canMoveHorizontal = true;
        canMoveVertical = true;

        /*if (PlayerPrefs.HasKey("SceneName")) PrevScene = PlayerController.PrevScene;
        if (SceneManager.GetActiveScene().name == "Foret_Polluee")
        {
            transform.position = new Vector3(-6.673989f, -2.097111f, -10);
        }

        if(SceneManager.GetActiveScene().name == "Village")
        {
            transform.position = new Vector3(58.53789f, -4.935112f, -9.01f);
        }*
    }

    // Update is called once per frame
    void Update()
{
    playerPosition = GameObject.Find("Protagoniste").transform.position;
    if (currentScene.name != "SceneBoss")
    {
        if (canMoveHorizontal && canMoveVertical)
        {
            // Add the current target position to the list of positions
            pointsInSpace.Enqueue(new PointInSpace() { Position = playerPosition, Time = Time.time });
            // Move the camera to the position of the target X seconds ago 
            while (pointsInSpace.Count > 0 && pointsInSpace.Peek().Time <= Time.time - delay + Mathf.Epsilon)
            {
                transform.position = Vector3.Lerp(new Vector3(playerPosition.x, playerPosition.y, playerPosition.z - 10), pointsInSpace.Dequeue().Position, Time.deltaTime * speed);
            }
            Camera.main.orthographicSize = 7f;
        }
        else if (!canMoveHorizontal && canMoveVertical)
        {
            // Add the current target position to the list of positions
            pointsInSpace.Enqueue(new PointInSpace() { Position = playerPosition, Time = Time.time });
            // Move the camera to the position of the target X seconds ago 
            while (pointsInSpace.Count > 0 && pointsInSpace.Peek().Time <= Time.time - delay + Mathf.Epsilon)
            {
                transform.position = Vector3.Lerp(new Vector3(transform.position.x, playerPosition.y, playerPosition.z - 10), pointsInSpace.Dequeue().Position, Time.deltaTime * speed);
            }
            Camera.main.orthographicSize = 7f;
        }
        else if (canMoveHorizontal && !canMoveVertical)
        {
            // Add the current target position to the list of positions
            pointsInSpace.Enqueue(new PointInSpace() { Position = playerPosition, Time = Time.time });
            // Move the camera to the position of the target X seconds ago 
            while (pointsInSpace.Count > 0 && pointsInSpace.Peek().Time <= Time.time - delay + Mathf.Epsilon)
            {
                transform.position = Vector3.Lerp(new Vector3(playerPosition.x, transform.position.y, playerPosition.z - 10), pointsInSpace.Dequeue().Position, Time.deltaTime * speed);
            }
            Camera.main.orthographicSize = 7f;
        }
        else
        {
            // Add the current target position to the list of positions
            pointsInSpace.Enqueue(new PointInSpace() { Position = playerPosition, Time = Time.time });
            // Move the camera to the position of the target X seconds ago 
            while (pointsInSpace.Count > 0 && pointsInSpace.Peek().Time <= Time.time - delay + Mathf.Epsilon)
            {
                transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, playerPosition.z - 10), pointsInSpace.Dequeue().Position, Time.deltaTime * speed);
            }
        }
    }
    else if (currentScene.name == "SceneBoss")
    {
        transform.position = new Vector3(0.12f, 1.65f, -10);
        Camera.main.orthographicSize = 8.5f;
    }

}
}*/
