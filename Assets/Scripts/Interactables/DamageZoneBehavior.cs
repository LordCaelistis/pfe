﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZoneBehavior : MonoBehaviour {

    private Rigidbody2D playerRigidbody2D;
    float cooldown_dmg_now = 0;
    public float cooldown_dmg_max = 1f;
    bool isInDamageZone = false;
	
	// Update is called once per frame
	void Update () {

        if (PlayerController.invincible == false && cooldown_dmg_now <= 0 && isInDamageZone)
        {
            GameObject.Find("Protagoniste").GetComponent<HealthScript>().TakeDamage();            
            cooldown_dmg_now += cooldown_dmg_max;
            PlayerController.cooldown_now_move += PlayerController.cooldown_max_move;
            playerRigidbody2D = GameObject.Find("Protagoniste").GetComponent<Rigidbody2D>();
            if (playerRigidbody2D.velocity.x != 0)
            {
                playerRigidbody2D.velocity += new Vector2(-(playerRigidbody2D.velocity.x) * 20, 0);
            }
            if (playerRigidbody2D.velocity.y != 0)
            {
                playerRigidbody2D.velocity += new Vector2(0, -(playerRigidbody2D.velocity.y) * 20);
            }
        }

        if (cooldown_dmg_now > 0) cooldown_dmg_now -= Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {        
        if (coll.gameObject.name == "Protagoniste")
        {
            isInDamageZone = true;
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Protagoniste")
        {
            isInDamageZone = false;
        }
    }
}
