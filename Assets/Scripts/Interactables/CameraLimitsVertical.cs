﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLimitsVertical : MonoBehaviour {

    public Camera mainCamera;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Protagoniste")
        {
            CameraControl.canMoveVertical = false;
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Protagoniste")
        {
            CameraControl.canMoveVertical = true;
        }
    }
}
