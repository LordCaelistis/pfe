﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vide : MonoBehaviour {

    bool isDansVide = false;
    private Rigidbody2D playerRigidbody2D;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (PlayerController.invincible == false && isDansVide) StartCoroutine(GameObject.Find("Protagoniste").GetComponent<PlayerController>().Death());
        if (PlayerController.invincible == true) Physics2D.IgnoreLayerCollision(0, 9, true);
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Protagoniste")
        {
            isDansVide = true;
            if (PlayerController.invincible == false)
            {
                coll.GetComponent<HealthScript>().TakeDamage();
                PlayerController.cooldown_now_move += PlayerController.cooldown_max_move;
                playerRigidbody2D = coll.GetComponent<Rigidbody2D>();
                if (playerRigidbody2D.velocity.x != 0)
                {
                    playerRigidbody2D.velocity += new Vector2(-(playerRigidbody2D.velocity.x) * 20, 0);
                }
                if (playerRigidbody2D.velocity.y != 0)
                {
                    playerRigidbody2D.velocity += new Vector2(0, -(playerRigidbody2D.velocity.y) * 20);
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Protagoniste")
        {
            isDansVide = false;
        }
    }


}
