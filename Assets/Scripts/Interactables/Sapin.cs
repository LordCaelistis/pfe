﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sapin : MonoBehaviour
{

    public SpriteRenderer spriteSapin;
	public Sprite Souche;
    private bool canCutTree = false;
    private bool treeIsCut = false;

    public AudioSource SourceSon;
    public AudioClip[] CoupeSap;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
           //Si le joueur est à portée pour couper le sapin, on change le sprite et on gagne une bûche
        if (Input.GetButtonDown("Melee") && canCutTree == true && treeIsCut == false)
        {
            GameObject.Find("Inventaire").GetComponent<InventaireItems>().GetBûche();
            canCutTree = false;
            treeIsCut = true;
			spriteSapin.GetComponent<SpriteRenderer>().sprite = Souche;
            SoundManager.PlayRandomOneShot(SourceSon, CoupeSap, 0.6f, 0.7f, 0.9f, 1.1f, 1f);
            GetComponent<Collider2D>().enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Protagoniste")
        {
            if (treeIsCut != true) canCutTree = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name == "Protagoniste")
        {
            if (treeIsCut != true) canCutTree = false;
        }
    }
}
