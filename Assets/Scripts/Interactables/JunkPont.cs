﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JunkPont : EnemyController {

    // Desired duration of the shake effect
    private float shakeDuration = 0f;
    // A measure of magnitude for the shake. Tweak based on your preference
    public float shakeMagnitude = 0.7f;
    // A measure of how quickly the shake effect should evaporate
    public float shakeFadeOut = 1.0f;


    // Use this for initialization
    new void Start () {
        maxHealth = 7;
        health = maxHealth;
        base.Start();
	}

    new void Update()
    {
        base.Update();

        if (shakeDuration > 0)
        {
            transform.localPosition = transform.position + Random.insideUnitSphere * shakeMagnitude;
            shakeDuration -= Time.deltaTime * shakeFadeOut;
        }
        else
        {
            shakeDuration = 0f;
        }
    }

    new void TakeDamage(float damageTaken)
    {
        base.TakeDamage(damageTaken);
        shakeDuration = 0.2f;
    }
}
