﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneColonnes : MonoBehaviour {

    public GameObject bossColonnes;
    public float duréeColonnes;
    public float cooldown_max_rune;
    private float cooldown_now_rune = 0;

    private IEnumerator coroutine;
    private bool inRange = false;

    void Update()
    {
            
        if (Input.GetButtonDown("Interagir") && inRange && cooldown_now_rune <= 0)
        {
            coroutine = Colonnes();
            StartCoroutine(coroutine);

            cooldown_now_rune += cooldown_max_rune;
        }

        if (cooldown_now_rune > 0) cooldown_now_rune -= Time.deltaTime;
    }

        //Les colonnes apparaissent puis disparaissent après une durée prédéfinie
    private IEnumerator Colonnes()
    {
        bossColonnes.SetActive(true);
        yield return new WaitForSeconds(duréeColonnes);
        bossColonnes.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        inRange = true;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        inRange = false;
    }
}
