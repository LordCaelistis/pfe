﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Teleport : MonoBehaviour {

    public string nameSceneToLoad;
    public bool mustBeActivated;
    private bool inRange = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
               //Si on interagit avec le téléporteur, on charge la scène prédéfinie
        if (Input.GetButtonDown("Interagir") && inRange)
        {
            SceneManager.LoadScene(nameSceneToLoad);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.name == "Protagoniste")
        {
            if (mustBeActivated == false) SceneManager.LoadScene(nameSceneToLoad);
            else inRange = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name == "Protagoniste")
        {
            inRange = false;
        }
    }
}
