﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapisRoulant : MonoBehaviour {

    public bool changerDirection = false;

    [Header("Touchez pas à ça")]
    public BoxCollider2D coll1;
    public BoxCollider2D coll2;

    Animator animator;

    // Use this for initialization
    void Start () {
        animator = gameObject.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
            //Gère que la direction soit inversée ou non (fonctionne en tandem avec un levier)
        if (changerDirection) GetComponent<AreaEffector2D>().forceAngle = 180;
        else GetComponent<AreaEffector2D>().forceAngle = 0;

        animator.SetBool("SensInverse", changerDirection);
    }

        //Si le joueur arrive sur le tapis roulant, on stoppe la vélocité du joueur pour le forcer à rester sur le tapis
    IEnumerator OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag == "Player")
        {
            yield return new WaitForSeconds(0.01f);
            coll.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            coll1.enabled = true;
            coll2.enabled = true;
            if (transform.eulerAngles.z == 0) PlayerController.canMoveHorizontal = false;
            if (transform.eulerAngles.z == 90) PlayerController.canMoveVertical = false;
        }
    }

        //On redonne au joueur sa liberté de mouvement une fois qu'il quitte le tapis roulant
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            coll1.enabled = false;
            coll2.enabled = false;
            if (transform.eulerAngles.z == 0) PlayerController.canMoveHorizontal = true;
            if (transform.eulerAngles.z == 90) PlayerController.canMoveVertical = true;
        }
    }

    /*IEnumerator OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            yield return new WaitForSeconds(0.05f);
            coll.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            isOnTapisRoulant = true;
            PlayerController.cooldown_now_dash = 5000;
            print(transform.eulerAngles.z);
            if (transform.eulerAngles.z == 0) PlayerController.canMoveHorizontal = false;
            if (transform.eulerAngles.z == 90) PlayerController.canMoveVertical = false;
            //PlayerController.cooldown_now_move = 5000;
        }
    }

    //On redonne au joueur sa liberté de mouvement une fois qu'il quitte le tapis roulant
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            print("coucou");
            isOnTapisRoulant = false;
            PlayerController.cooldown_now_dash = 0;
            if (transform.eulerAngles.z == 0) PlayerController.canMoveHorizontal = true;
            if (transform.eulerAngles.z == 90) PlayerController.canMoveVertical = true;
            //PlayerController.cooldown_now_move = 0;
        }
    }*/
}
