﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaissePickup : MonoBehaviour {

    public SpriteRenderer spriteSapin;
    private bool canCutTree = false;
    private bool treeIsCut = false;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //Si le joueur est à portée pour couper le sapin, on change le sprite et on gagne une bûche
        if (Input.GetButtonDown("Interagir") && canCutTree == true && treeIsCut == false)
        {
            GameObject.Find("Inventaire").GetComponent<InventaireItems>().GetBûche();
            treeIsCut = true;
            spriteSapin.color = Color.red;
            //SoundManager.PlayRandomOneShot(HuDBoîteDialogue, HuD, 0.7f);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        canCutTree = true;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        canCutTree = false;
    }
}
