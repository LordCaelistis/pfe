﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLimitHorizontal : MonoBehaviour {

    public Camera mainCamera;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.name == "Protagoniste")
        {
            CameraControl.canMoveHorizontal = false;
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Protagoniste")
        {
            CameraControl.canMoveHorizontal = true;
        }
    }
}
