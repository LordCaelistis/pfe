﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

    public Sprite lanterneAllumee;
    public ParticleSystem effect;

        //Quand on passe le checkpoint, on déplace la position du SpawnPlayer
        //Ca modifie donc sa position de respawn
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Protagoniste")
        {
            GameObject.Find("PlayerSpawn").transform.position = transform.position;
            col.gameObject.GetComponent<HealthScript>().RefillHealth();
            GetComponentInChildren<SpriteRenderer>().sprite = lanterneAllumee;
            effect.Play();
            Instantiate(effect, GameObject.Find("SpawnLum").transform.position, transform.rotation);
        }            
    }
}
