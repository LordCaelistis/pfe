﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SablesMouvants : MonoBehaviour {

    private GameObject instancePlayer;

    //Narration
    bool joueurDedans;

    //Ennemi lié
    public GameObject ennemiAssocie;
    public GameObject ennemiMeleePrefab;
    public GameObject ennemiDistancePrefab;

    //cooldown spawn
    public float cooldownMax = 4f;
    float cooldownNow;

    //Détection
    public SpawnEnnemi detectPlayer;

    // Use this for initialization
    public void Start ()
    {
        cooldownNow = 0;
        // RandomEnnemi = Random.Range(0, 100);
    }
	
	// Update is called once per frame
	public void Update ()
    {
        if (detectPlayer.spawn && AvancementQuete.GGWP == false)
        {
            if (cooldownNow <= 0)
            {
                Spawn();
                cooldownNow += cooldownMax;
            }
            else cooldownNow -= Time.deltaTime;
        }
    }

        //On divise la vitesse du joueur par deux
    void OnTriggerEnter2D(Collider2D coll)
    {
        joueurDedans = true;
        PlayerController.vitesse = PlayerController.vitesseBase / 2;
    }

        //On remet la vitesse du joueur à la normale
    void OnTriggerExit2D(Collider2D coll)
    {
        joueurDedans = false;
        PlayerController.vitesse = PlayerController.vitesseBase;
    }

        //Code Ennemi Spawn
    void Spawn()
    {
        if(AvancementQuete.PriseDeConscience && joueurDedans == false && ennemiAssocie == null && AvancementQuete.GGWP == false)
        {
            Vector3 posEnnemi = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            Quaternion rotationEnnemi = Quaternion.identity;

            if (Random.Range(0, 100) >= 40)
            {
                ennemiAssocie = (GameObject)Instantiate(ennemiMeleePrefab, posEnnemi, rotationEnnemi);
            }
            else
            {
                ennemiAssocie = (GameObject)Instantiate(ennemiDistancePrefab, posEnnemi, rotationEnnemi);
            }
        }
        
    }
}
