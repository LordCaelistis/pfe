﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levier : MonoBehaviour {

    public AudioSource SourceSon;
    public GameObject tapisRoulant;
    public AudioClip[] InterLevier;

        //A l'impact, on inverse le sens du levier et on change la direction du tapis roulant associé
    void OnCollisionEnter2D(Collision2D coll)
    {
        SoundManager.PlayRandomOneShot(SourceSon, InterLevier, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
        GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
        tapisRoulant.GetComponent<TapisRoulant>().changerDirection = !tapisRoulant.GetComponent<TapisRoulant>().changerDirection;
    }
}
