﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SoundManager : object {

    /*
    1) Créer une audio source dans le prefab
    2) Mettre dans le script de l'objet deux éléments (avant le Start):
    ---> public AudioSource *le nom de ma source*;
    ---> public AudioClip[] *le nom de ma liste de sons*;
    Ça permettra d'utiliser dans le script la fonction suivante :

    SoundManager.PlayRandomOneShot(*le nom de ma source*, *le nom de ma liste de sons*, *le volume*);

    // Paramètres : Nom de la source | Nom de la liste de sons | Volume min | Volume max | Pitch min | Pitch max | Proba
    SoundManager.PlayRandomOneShot(soundSource, soundList, 0.8, 1, 0.9, 1.1, 1);

    Il faut placer le morceau de code ci dessus à l'endroit du code correspondant
    à l'action pour laquelle on fait le feedback.

    Ensuite, il suffira de glisser déposer les sons dans l'inspecteur d'Unity
    */

    public static void PlayRandomOneShot(
        AudioSource source,
        AudioClip[] sounds,
        float vol
    ){PlayRandomOneShot(source,sounds,vol,vol,1,1,1);}

    // public static void PlayRandomOneShot(
    //     AudioSource source,
    //     AudioClip[] sounds,
    //     float[] vol = {1f,1f},
    //     float[] pitch  = {1f,1f},
    //     float   proba  = 1f
    // ){PlayRandomOneShot(source,sounds,vol[0],vol[1],pitch[0],pitch[1],proba);}

    // Complete function
    public static void PlayRandomOneShot(
        AudioSource source,
        AudioClip[] sounds,
        float volMin = 1f,
        float volMax = 1f,
        float pitchMin = 1f,
        float pitchMax = 1f,
        float proba = 1f
    ){
        if(Random.value <= proba && sounds.Length > 0) {
            int chosenOne = Random.Range (0, sounds.Length);

            source.clip   = sounds[chosenOne];
            source.volume = Random.Range (volMin,volMax);
            source.pitch  = Random.Range (pitchMin,pitchMax);

            source.Play();
        }
    }
}
