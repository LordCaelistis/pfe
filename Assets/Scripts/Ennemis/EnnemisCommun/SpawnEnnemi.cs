﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnnemi : MonoBehaviour {

    public GameObject instancePlayer;

    //Narration
    public bool spawn = false;

	// Use this for initialization
	public void Start ()
    {

    }

    // Update is called once per frame
    public void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Protagoniste")
        {
            spawn = true;
            instancePlayer = GameObject.Find("Protagoniste");
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Protagoniste")
        {
            spawn = false;
        }
    }
}
