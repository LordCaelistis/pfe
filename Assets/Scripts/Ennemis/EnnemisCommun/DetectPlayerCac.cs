﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayerCac : MonoBehaviour {

    private GameObject instancePlayer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        // C'est bon, il est détecté
        if (collision.name == "Protagoniste")
        {
            transform.parent.gameObject.GetComponent<EnnemiCac>().agressif = true;
            instancePlayer = GameObject.Find("Protagoniste");
            //print("il est là");
            transform.parent.gameObject.GetComponent<EnnemiCac>().cooldown_now_atk = 0f;
        }
    }

    // Quand le joueur sort de la zone de détection
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Protagoniste")
        {
            transform.parent.gameObject.GetComponent<EnnemiCac>().agressif = false;
            instancePlayer = GameObject.Find("Protagoniste");
            //print("il est partie");
        }
    }
}
