﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayerDistance : MonoBehaviour {

    private GameObject instancePlayer;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        // C'est bon, il est détecté
        if (collision.name == "Protagoniste")
        {
            transform.parent.gameObject.GetComponent<EnnemiDistance>().agressif = true;
            instancePlayer = GameObject.Find("Protagoniste");
            //print("il est là");
            transform.parent.gameObject.GetComponent<EnnemiDistance>().cooldown_now_atk = 0f;
        }
    }

    // Quand le joueur sort de la zone de détection
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Protagoniste")
        {
            transform.parent.gameObject.GetComponent<EnnemiDistance>().agressif = false;
            instancePlayer = GameObject.Find("Protagoniste");
            //print("il est partie");
        }
    }
}
