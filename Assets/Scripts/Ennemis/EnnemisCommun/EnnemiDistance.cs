﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiDistance : EnemyController
{
    public GameObject TirEnnemiPrefab;
    private GameObject instancePlayer;

    // Variable détection:
    public Collider2D zonePatrouille;
    public Rigidbody2D ennemyBody;

    // Caractéristiques Ennemi:
    public float speed = 30.0f;
    public float distance;

    // Action ennemi;
    public float cooldown_max_atk = 30f;
    public float cooldown_now_atk = 0f;
    public float distanceDamage;

    // Comportement
    public bool agressif = false;

    public AudioSource SonTir;
    public AudioClip sonTirEnnemi;    

    public override void Start()
    {
        instancePlayer = GameObject.Find("Protagoniste");
        ennemyBody = GetComponent<Rigidbody2D>();

        cooldown_max_atk = 2f;
        canAct = true;
        base.Start();
    }

    public override void Update()
    {
        if (agressif && canAct)
        {
            //print("Grr");
            //fait ton grr
            Vector2 posRelativeJoueur = instancePlayer.transform.position - gameObject.transform.position;
            posRelativeJoueur = posRelativeJoueur.normalized;
            ennemyBody.velocity += posRelativeJoueur * speed * Time.deltaTime;

            //ennemyBody.AddForce((gameObject.transform.position - instancePlayer.transform.position).normalized * speed * Time.deltaTime);
            //ATTENTION AU Z

            if (cooldown_now_atk <= 0)
            {
                Vector3 posTirEnnemi = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                Quaternion rotationTirEnnemi = Quaternion.identity;
                GameObject TirEnnemi = (GameObject)Instantiate(TirEnnemiPrefab, posTirEnnemi, rotationTirEnnemi);
                SonTir.Play();


                //print("tir");
                cooldown_now_atk += cooldown_max_atk;
                print(cooldown_max_atk);
            }

            //print(cooldown_now_atk);
            if (cooldown_now_atk > 0) cooldown_now_atk -= Time.deltaTime;
        }



        base.Update();
    }
    // L'ennemi doit marcher vers le Joueur lorsque le Joueur entre dans la Zone Trigger "ZoneDetection":
    
}
