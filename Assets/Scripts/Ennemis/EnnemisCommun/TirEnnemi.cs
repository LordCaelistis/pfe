﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TirEnnemi : MonoBehaviour
{
    public GameObject TirEnnemiPrefab;
    private GameObject instancePlayer;
    private Vector3 positionJoueur;
    private Vector3 posBalleOriginal;
    

    // Paramètre de la balle
    float vitesse = 10f;
    float timerLifetimeBalle_max = 3.2f;
    float timerLifetimeBalle_now = 0;
    bool touchProta;

    // Use this for initialization
    void Start ()
    {
        instancePlayer = GameObject.Find("Protagoniste");
        positionJoueur = instancePlayer.transform.position;
        posBalleOriginal = gameObject.transform.position;
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Missiles à tête chercheuse:
        timerLifetimeBalle_now += Time.deltaTime;
        Vector3 posRelativeJoueur = positionJoueur - posBalleOriginal;
        posRelativeJoueur = posRelativeJoueur.normalized;
        //transform.position += positionJoueur.normalized * vitesse * Time.deltaTime;
        transform.position += posRelativeJoueur * vitesse * Time.deltaTime;
        

        //new Vector3 

        if (touchProta)
        {
            GameObject.Find("Protagoniste").GetComponent<HealthScript>().TakeDamage();
            touchProta = false;
        }

        if (timerLifetimeBalle_now >= timerLifetimeBalle_max)
        {
            Destroy(gameObject);
            timerLifetimeBalle_now = 0;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        print("je touche un truc");
        //print(collision.gameObject.name);
        //print(collision.gameObject.layer);

        if (collision.gameObject.layer == 11)
        {
            Physics2D.IgnoreCollision(collision, GetComponent<Collider2D>());
            if (PlayerController.invincible == false)
            {
                //collision.GetComponent<HealthScript>().TakeDamage();
            }
        }

        if (collision.name == "Protagoniste")
        {
            touchProta = true;
            instancePlayer = GameObject.Find("Protagoniste");
            print("touché !");
        }

        if (collision.gameObject.layer == 10)
        {
            print(collision.gameObject.name);
            Destroy(gameObject);
        }
    }
}
