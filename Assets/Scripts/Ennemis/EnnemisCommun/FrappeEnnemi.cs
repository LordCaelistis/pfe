﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrappeEnnemi : MonoBehaviour {

    public GameObject instancePlayer;

    // Comportement:
    public bool frappe = false;

	// Use this for initialization
	public void Start ()
    {
        //ennemyBody = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	public  void Update ()
    {
   
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.name == "Protagoniste")
        {
            frappe = true;
            instancePlayer = GameObject.Find("Protagoniste");
            print("je te frappe");
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.name == "Protagoniste")
        {
            frappe = false;
            print("je ne te frappe plus");
        }
    }
}
