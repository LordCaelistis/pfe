﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiCac : EnemyController {

    private GameObject instancePlayer;

    // Variable détection:
    public Collider2D zonePatrouille;
    public Rigidbody2D ennemyBody;

    // Caractéristiques Ennemi:
    public float speed = 15.0f; 
    public float distance;

    // Action ennemi;
    public FrappeEnnemi zoneFrappe;
    public float cooldown_max_atk = 20f;
    public float cooldown_now_atk = 0f;
    public float meleeDamage;

    // Comportement
    public bool agressif = false;
    

    public override void Start()
    {
        instancePlayer = GameObject.Find("Protagoniste");
        ennemyBody = GetComponent<Rigidbody2D>();
        cooldown_max_atk = 1f;
        base.Start();
    }

    public override void Update()
    {
        if (agressif && canAct)
        {           
            //print("Grr");
            //fait ton grr
            Vector2 posRelativeJoueur = instancePlayer.transform.position - gameObject.transform.position;
            posRelativeJoueur = posRelativeJoueur.normalized;
            ennemyBody.velocity += posRelativeJoueur * speed * Time.deltaTime;
            //ennemyBody.AddForce((gameObject.transform.position - instancePlayer.transform.position).normalized * speed * Time.deltaTime);
            //ATTENTION AU Z

            if (zoneFrappe.frappe)
            {
                if (cooldown_now_atk <= 0)
                {
                    //print("attaque");
                    GameObject.Find("Protagoniste").GetComponent<HealthScript>().TakeDamage();
                    cooldown_now_atk += cooldown_max_atk;
                    print(cooldown_max_atk);

                    if (visualBodyAnimator != null) visualBodyAnimator.SetTrigger("Strike");
                }

                //print(cooldown_now_atk);
                if(cooldown_now_atk > 0) cooldown_now_atk -= Time.deltaTime;                
            }
        }

        if (visualBodyAnimator != null) visualBodyAnimator.SetFloat("vitesse", ennemyBody.velocity.magnitude);
        base.Update();
    }
    // L'ennemi doit marcher vers le Joueur lorsque le Joueur entre dans la Zone Trigger "ZoneDetection":
    
}
