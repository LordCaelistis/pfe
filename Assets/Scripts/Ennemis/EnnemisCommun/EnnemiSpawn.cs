﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiSpawn : MonoBehaviour {

    [SerializeField] GameObject TestOgre;
    [SerializeField] GameObject EnnemiDistance;
    bool OgreInvoqué = false;

    //GameObject Ogre;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Invocation Préfab: 
   
    void OnTriggerEnter2D(Collider2D collision)
    {
        //print("jentre");
        //print(Random.value);
        if (Random.value>=0.4)
        {
            Vector3 posTestOgre = new Vector3(14, 3, transform.position.z);
            Quaternion rotationTestOgre = Quaternion.identity; 
            GameObject Ogre = (GameObject)Instantiate(TestOgre, posTestOgre, rotationTestOgre);
            //OgreInvoqué = true;
        }
        else
        {
            Vector3 posEnnemiDistance = new Vector3(14, 3, transform.position.z);
            Quaternion rotationEnnemiDistance = Quaternion.identity;
            GameObject EnnDistance = (GameObject)Instantiate(EnnemiDistance, posEnnemiDistance, rotationEnnemiDistance);
            //OgreInvoqué = true;

        }
        
    }


}
