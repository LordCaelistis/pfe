﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour {

    [Header("Generic")]
    public GameObject visualBody;
    public Animator visualBodyAnimator;
    public ParticleSystem effect;
    private Rigidbody2D rigidbodyEnnemi;

    public float health;
    public float maxHealth;
    public Image bossHealthbar;

    private static float canAct_cooldown_now = 0f;
    [SerializeField]
    public static float canAct_cooldown_max = 0.5f;
    public bool canAct = true;

    public virtual void Start()
    {
        rigidbodyEnnemi = GetComponent<Rigidbody2D>();
        if (visualBody != null && visualBodyAnimator == null){
            try {
                visualBodyAnimator = visualBody.GetComponent<Animator>();
            } catch {
                visualBodyAnimator = null;
            }
        }
    }

    public virtual void Update()
    {
        if (canAct_cooldown_now > 0) canAct_cooldown_now -= Time.deltaTime;
        else canAct = true;
    }

    public static void takeHitstun()
    {
        canAct_cooldown_now += canAct_cooldown_max;
    }

    public virtual void TakeDamage(float damageReceived)
    {
        health -= damageReceived;
        takeHitstun();

        effect.Play();
        Instantiate(effect, transform.position, transform.rotation);

        if (transform.name != "Boss") rigidbodyEnnemi.velocity = new Vector2(-(rigidbodyEnnemi.velocity.x) * 5, -(rigidbodyEnnemi.velocity.y) * 5);

        if (visualBodyAnimator != null) visualBodyAnimator.SetTrigger("Hit");

        if (transform.name == "Boss")
        {
            bossHealthbar.transform.localScale = new Vector3(health / 100, 1f, 1f);
        }

        if (health <= 0 && transform.name == "Boss")
        {
            Time.timeScale = 0.3f;
            VictoryFadeScreen.FadeIn = true;
            StartCoroutine(LoadEcranFin());
        }
        else if (health <= 0 && transform.name == "MachinePrefab")
        {
            StartCoroutine(GameObject.Find("ScriptNiveau").GetComponent<NiveauForet>().CutsceneDefeatBoss());
            transform.gameObject.GetComponent<EnemyController>().health = 50000;

            AvancementQuete.GGWP = true;
            print(AvancementQuete.GGWP);
            AvancementQuete.responsable = true;
            AvancementQuete.QueteTerminee = true;

            foreach (GameObject ennemi in GameObject.FindGameObjectsWithTag("Ennemis"))
            {
                Destroy(ennemi);
            }
        }
        else if (health <= 0 && transform.name != "MachinePrefab" || health <= 0 && transform.name != "Boss")
        {
            Destroy(gameObject);
        }
    }

    IEnumerator LoadEcranFin()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("EcranFin");
    }
}
