﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeExplosion : MonoBehaviour {

    float timer_max = 0.5f;
    float timer = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer = timer + Time.deltaTime;
        if(timer >= timer_max)
        {
            Destroy(gameObject);
        }
	}
}
