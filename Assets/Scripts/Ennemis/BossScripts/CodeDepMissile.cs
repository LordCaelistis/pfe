﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeDepMissile : MonoBehaviour {
    [SerializeField] GameObject Boum;
    // Use this for initialization
    void Start () {
        
    }

    // Update is called once per frame
    void Update() {
        //----------------------FAIRE AVANCER MON MISSILE-----------------------------
        transform.position = transform.position + new Vector3(1, 0, 0) * 10 * Time.deltaTime;
    }


    //-----------------QUAND MON MISSILE ENTRE EN COLLISION------------------------------//
    void OnTriggerEnter2D(Collider2D collision) {
        print("Joueur : Un de moins !");

        if (collision.gameObject.tag != "Projectiles")
        {
            Destroy(collision.gameObject);
            Vector3 posExplosion = new Vector3(gameObject.transform.position.x +1, gameObject.transform.position.y, gameObject.transform.position.z);
            Quaternion rotExplosion = Quaternion.identity;
            GameObject boum = (GameObject)Instantiate(Boum, posExplosion, rotExplosion);
        }
    }
}