﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CodeBoss : EnemyController {

    [Header("Boss specific")]
    [SerializeField] GameObject ennemi;
    [SerializeField] GameObject ProjectileEnnemiPrefab;
    [SerializeField] GameObject ZoneSafeEnnemiPrefab;
    [SerializeField] GameObject ZoneEnnemiPrefab;
    [SerializeField] GameObject BombeEnnemiPrefab;
    public GameObject laserSafe;
    public GameObject laser;

    public AudioSource AudioBullet;
    public AudioSource AudioZone;
    public AudioSource AudioExplosion;
    public AudioSource AudioExplosion2;
    public AudioClip[] TirBullet;
    public AudioClip[] Explosions;
    public AudioClip[] DegatRecuBoss;
    public AudioClip[] ZoneCone1;
    public AudioClip[] ZoneCone2;
    public AudioClip[] SmokeBossLoop;
    public AudioSource BossEndom;
    public AudioClip[] SteamBoss;

    public Animator animatorMainGauche;
    public Animator animatorMainDroite;
    // animatorMainGauche.SetTrigger("Shoot");
    // animatorMainDroite.SetTrigger("Shoot");

    public GameObject spawnMainGauche;
    public GameObject spawnMainDroite;
    public GameObject spawnBombeGauche;
    public GameObject spawnBombeDroite;
    public Vector3 spawnMainGauchePosition;
    public Vector3 spawnMainDroitePosition;

    float duréeSalveBalle = 2f;
    float lancerSalveDroiteBalle;
    float timerSalveGaucheBalle = 0;
    float timerSalveDroiteBalle = 0;
    float timerPatternBalle = 0;
    float duréePatternBalle = 10f;
    public static bool fireSecondWave = false;

    bool horizontal = true;
    float timerZoneSafe_now = 2;
    float timerZoneSafe_max = 3.5f;
    bool zoneSafeAppeared = false;
    float timerZone_now = 0;
    float timerZone_max = 1.5f;
    float timerPatternZone_now = 0;
    float timerPatternZone_max = 20f;

    float timerLaserSafe_now = 2;
    float timerLaserSafe_max = 3.5f;
    bool LaserSafeAppeared = false;
    float timerLaser_now = 0;
    float timerLaser_max = 1.5f;
    float timerPatternLaser_now = 0;
    float timerPatternLaser_max = 10f;

    float timerASbombe_now = 0;
    float timerASbombe_max = 2;
    float délaiEntreBombes = 0.5f;
    bool patternBombesSwitch = false;
    float timerPatternBombe_now = 0;
    float timerPatternBombe_max = 15f;
    bool thirdBomb = false;

    bool isPatternBalles = false;
    bool isPatternZones = false;
    bool isPatternBombes = false;
    bool isPatternLasers = false;

    int numberOfPatterns = 3;
    float cooldownPattern = 0;
    bool canLaunchPattern = true;
    int lastPatternIdentifier = 1;
    IEnumerator coroutine;

    public bool canAttack = true;

    float timeSincePatternStarted = 0;

    new void Start()
    {
        base.Start();
        maxHealth = GameObject.Find("Protagoniste").GetComponent<AvancementQuete>().bossMaxHealth;
        spawnMainGauchePosition = spawnMainGauche.transform.position;
        spawnMainDroitePosition = spawnMainDroite.transform.position;
        lancerSalveDroiteBalle = duréeSalveBalle / 2;
        CallNewPattern();
    }

    // Update is called once per frame
    new void Update()
    {
        if (Input.GetKeyDown("m"))
        {
            health = 5;
        }

        Chronometre();

        if (isPatternBalles) PatternBalles();
        if (isPatternZones) PatternZones();
        if (isPatternBombes)
        {
            coroutine = PatternBombes();
            StartCoroutine(coroutine);
        }
        //if (isPatternLasers) PatternLasers();

        if (cooldownPattern >= 0) cooldownPattern -= Time.deltaTime;
        if (health < maxHealth / 4)
        {
            //A partir du moment ou le boss à 25% de sa vie, jouer le son en loop (même si la loop je peux la gerer sous unity)
            // Paramètres : Nom de la source | Nom de la liste de sons | Volume min | Volume max | Pitch min | Pitch max | Proba
            SoundManager.PlayRandomOneShot(BossEndom, SteamBoss, 0.1f, 0.2f, 0.9f, 1.1f, 1f);
        }
    }

    void PatternBalles()
    {
        //print("BALLES !");
        timerPatternBalle = timerPatternBalle + Time.deltaTime;
        timerSalveGaucheBalle = timerSalveGaucheBalle + Time.deltaTime;
        timerSalveDroiteBalle = timerSalveDroiteBalle + Time.deltaTime;
        //print(timerSalveDroiteBalle);
        /*-------------------------CRÉATION DE BALLES-----------------------------------*/
        if (timerSalveGaucheBalle >= duréeSalveBalle)
        {
            animatorMainGauche.SetTrigger("Shoot");
            timerSalveGaucheBalle = 0;
            fireSecondWave = false;
            Vector3 posBallesEnnemi7 = new Vector3(spawnMainGauchePosition.x, spawnMainGauchePosition.y, spawnMainGauchePosition.z);
            Vector3 posBallesEnnemi1 = new Vector3(spawnMainGauchePosition.x, spawnMainGauchePosition.y, spawnMainGauchePosition.z);
            Vector3 posBallesEnnemi4 = new Vector3(spawnMainGauchePosition.x, spawnMainGauchePosition.y, spawnMainGauchePosition.z);
            Vector3 posBallesEnnemi2 = new Vector3(spawnMainGauchePosition.x, spawnMainGauchePosition.y, spawnMainGauchePosition.z);
            Vector3 posBallesEnnemi8 = new Vector3(spawnMainGauchePosition.x, spawnMainGauchePosition.y, spawnMainGauchePosition.z);
            Quaternion rotBallesEnnemi1 = Quaternion.Euler(0, 0, -60f);
            Quaternion rotBallesEnnemi2 = Quaternion.Euler(0, 0, -75f);
            Quaternion rotBallesEnnemi7 = Quaternion.Euler(0, 0, -90);
            Quaternion rotBallesEnnemi4 = Quaternion.Euler(0, 0, -105);
            Quaternion rotBallesEnnemi8 = Quaternion.Euler(0, 0, -120);
            GameObject balle1 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi1, rotBallesEnnemi1);
            GameObject balle2 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi2, rotBallesEnnemi2);
            GameObject balle4 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi4, rotBallesEnnemi4);
            GameObject balle7 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi7, rotBallesEnnemi7);
            GameObject balle8 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi8, rotBallesEnnemi8);
            SoundManager.PlayRandomOneShot(AudioBullet, TirBullet, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
        }
        if (fireSecondWave == true)
        {
            animatorMainDroite.SetTrigger("Shoot");
            fireSecondWave = false;
            Vector3 posBallesEnnemi7 = new Vector3(spawnMainDroitePosition.x, spawnMainDroitePosition.y, spawnMainDroitePosition.z);
            Vector3 posBallesEnnemi1 = new Vector3(spawnMainDroitePosition.x, spawnMainDroitePosition.y, spawnMainDroitePosition.z);
            Vector3 posBallesEnnemi4 = new Vector3(spawnMainDroitePosition.x, spawnMainDroitePosition.y, spawnMainDroitePosition.z);
            Vector3 posBallesEnnemi2 = new Vector3(spawnMainDroitePosition.x, spawnMainDroitePosition.y, spawnMainDroitePosition.z);
            Vector3 posBallesEnnemi8 = new Vector3(spawnMainDroitePosition.x, spawnMainDroitePosition.y, spawnMainDroitePosition.z);
            Quaternion rotBallesEnnemi1 = Quaternion.Euler(0, 0, -60f);
            Quaternion rotBallesEnnemi2 = Quaternion.Euler(0, 0, -75f);
            Quaternion rotBallesEnnemi7 = Quaternion.Euler(0, 0, -90);
            Quaternion rotBallesEnnemi4 = Quaternion.Euler(0, 0, -105);
            Quaternion rotBallesEnnemi8 = Quaternion.Euler(0, 0, -120);
            GameObject balle1 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi1, rotBallesEnnemi1);
            GameObject balle2 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi2, rotBallesEnnemi2);
            GameObject balle4 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi4, rotBallesEnnemi4);
            GameObject balle7 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi7, rotBallesEnnemi7);
            GameObject balle8 = (GameObject)Instantiate(ProjectileEnnemiPrefab, posBallesEnnemi8, rotBallesEnnemi8);
            SoundManager.PlayRandomOneShot(AudioBullet, TirBullet, 0.2f, 0.3f, 0.9f, 1.1f, 1f);
        }
    }

    void PatternZones()
    {
        //print("ZONES !");
        timerPatternZone_now = timerPatternZone_now + Time.deltaTime;
        timerZoneSafe_now = timerZoneSafe_now + Time.deltaTime;
        if (horizontal)
        {
            /*-------------------------CRÉATION DE ZONE SAFE HORIZONTAL-----------------------------------*/
            if (timerZoneSafe_now >= timerZoneSafe_max)
            {
                timerZone_now = timerZone_now + Time.deltaTime;
                if (!zoneSafeAppeared)
                {
                    zoneSafeAppeared = true;
                    Vector3 posZoneSafeEnnemi1 = new Vector3(-5.5f, -1f, transform.position.z);
                    Vector3 posZoneSafeEnnemi2 = new Vector3(spawnMainDroitePosition.x, -1f, transform.position.z);
                    Quaternion rotZoneSafeEnnemi1 = Quaternion.identity;
                    Quaternion rotZoneSafeEnnemi2 = Quaternion.identity;
                    GameObject zone1 = (GameObject)Instantiate(ZoneSafeEnnemiPrefab, posZoneSafeEnnemi1, rotZoneSafeEnnemi1);
                    GameObject zone2 = (GameObject)Instantiate(ZoneSafeEnnemiPrefab, posZoneSafeEnnemi2, rotZoneSafeEnnemi2);
                    SoundManager.PlayRandomOneShot(AudioZone, ZoneCone1, 0.4f, 0.5f, 0.9f, 1.1f, 1f);
                }
                /*-------------------------CRÉATION DE ZONE DANGEREUSE HORIZONTAL-----------------------------------*/
                if (timerZone_now >= timerZone_max)
                {
                    zoneSafeAppeared = false;
                    timerZoneSafe_now = 0;
                    timerZone_now = 0;
                    Vector3 posZoneEnnemi1 = new Vector3(-5.5f, - 1f, transform.position.z);
                    Vector3 posZoneEnnemi2 = new Vector3(spawnMainDroitePosition.x, - 1f, transform.position.z);
                    Quaternion rotZoneEnnemi1 = Quaternion.identity;
                    Quaternion rotZoneEnnemi2 = Quaternion.identity;
                    GameObject zone3 = (GameObject)Instantiate(ZoneEnnemiPrefab, posZoneEnnemi1, rotZoneEnnemi1);
                    GameObject zone4 = (GameObject)Instantiate(ZoneEnnemiPrefab, posZoneEnnemi2, rotZoneEnnemi2);
                    horizontal = !horizontal;
                    SoundManager.PlayRandomOneShot(AudioZone, ZoneCone2, 0.4f, 0.5f, 0.9f, 1.1f, 1f);
                }
            }
        }
        else
        {
            /*-------------------------CRÉATION DE ZONE SAFE VERTICAL-----------------------------------*/
            if (timerZoneSafe_now >= timerZoneSafe_max)
            {
                timerZone_now = timerZone_now + Time.deltaTime;
                if (!zoneSafeAppeared)
                {
                    zoneSafeAppeared = true;
                    Vector3 posZoneSafeEnnemi1 = new Vector3(0.05f, 1.25f, transform.position.z);
                    Quaternion rotZoneSafeEnnemi1 = Quaternion.identity;
                    GameObject zone1 = (GameObject)Instantiate(ZoneSafeEnnemiPrefab, posZoneSafeEnnemi1, rotZoneSafeEnnemi1);
                    SoundManager.PlayRandomOneShot(AudioZone, ZoneCone1, 0.4f, 0.5f, 0.9f, 1.1f, 1f);
                }
                /*-------------------------CRÉATION DE ZONE DANGEREUSE VERTICAL-----------------------------------*/
                if (timerZone_now >= timerZone_max)
                {
                    zoneSafeAppeared = false;
                    timerZoneSafe_now = 0;
                    timerZone_now = 0;
                    Vector3 posZoneEnnemi1 = new Vector3(0.05f, 1.25f, transform.position.z);
                    Quaternion rotZoneEnnemi1 = Quaternion.identity;
                    GameObject zone3 = (GameObject)Instantiate(ZoneEnnemiPrefab, posZoneEnnemi1, rotZoneEnnemi1);
                    horizontal = !horizontal;
                    SoundManager.PlayRandomOneShot(AudioZone, ZoneCone2, 0.4f, 0.5f, 0.9f, 1.1f, 1f);
                }
            }
        }
    }

    IEnumerator PatternBombes()
    {
        timerASbombe_now = timerASbombe_now + Time.deltaTime;
        /*-------------------------CRÉATION DE BOMBE-----------------------------------*/
        if (timerASbombe_now >= timerASbombe_max * 0.7f && timerASbombe_now < timerASbombe_max * 0.705f && patternBombesSwitch == false)
        {
            thirdBomb = true;
            Vector3 posBombeEnnemi1 = new Vector3(spawnMainGauchePosition.x, spawnMainGauchePosition.y, transform.position.z);
            Vector3 posBombeEnnemi2 = new Vector3(spawnMainDroitePosition.x, spawnMainDroitePosition.y, transform.position.z);
            Quaternion rotBombeEnnemi = Quaternion.Euler(0, 0, 0);
            GameObject bombe1 = (GameObject)Instantiate(BombeEnnemiPrefab, posBombeEnnemi1, rotBombeEnnemi);
            GameObject bombe2 = (GameObject)Instantiate(BombeEnnemiPrefab, posBombeEnnemi2, rotBombeEnnemi);
            SoundManager.PlayRandomOneShot(AudioExplosion, Explosions, 0.4f, 0.5f, 0.9f, 1f, 1f);
        }
        else if (timerASbombe_now >= timerASbombe_max && patternBombesSwitch == false && thirdBomb == true)
        {
            Vector3 posBombeEnnemi3 = new Vector3(0, spawnBombeGauche.transform.position.y, transform.position.z);
            Quaternion rotBombeEnnemi = Quaternion.Euler(0, 0, 0);
            GameObject bombe3 = (GameObject)Instantiate(BombeEnnemiPrefab, posBombeEnnemi3, rotBombeEnnemi);
            thirdBomb = false;
            yield return new WaitForSeconds(délaiEntreBombes);
            timerASbombe_now = 0;
            patternBombesSwitch = !patternBombesSwitch;
            SoundManager.PlayRandomOneShot(AudioExplosion2, Explosions, 0.4f, 0.5f, 0.9f, 1f, 1f);
        }
        else if (timerASbombe_now >= timerASbombe_max * 0.7f && timerASbombe_now < timerASbombe_max * 0.705f && patternBombesSwitch == true)
        {
            thirdBomb = true;
            Vector3 posBombeEnnemi1 = new Vector3(spawnBombeGauche.transform.position.x, spawnBombeGauche.transform.position.y, transform.position.z);
            Vector3 posBombeEnnemi2 = new Vector3(spawnBombeDroite.transform.position.x, spawnBombeDroite.transform.position.y, transform.position.z);
            Quaternion rotBombeEnnemi = Quaternion.Euler(0, 0, 0);
            GameObject bombe1 = (GameObject)Instantiate(BombeEnnemiPrefab, posBombeEnnemi1, rotBombeEnnemi);
            GameObject bombe2 = (GameObject)Instantiate(BombeEnnemiPrefab, posBombeEnnemi2, rotBombeEnnemi);
            SoundManager.PlayRandomOneShot(AudioExplosion2, Explosions, 0.4f, 0.5f, 0.9f, 1f, 1f);
        }
        else if (timerASbombe_now >= timerASbombe_max && patternBombesSwitch == true && thirdBomb == true)
        {
            thirdBomb = false;
            Vector3 posBombeEnnemi3 = new Vector3(0, spawnMainGauchePosition.y, transform.position.z);
            Quaternion rotBombeEnnemi = Quaternion.Euler(0, 0, 0);
            GameObject bombe3 = (GameObject)Instantiate(BombeEnnemiPrefab, posBombeEnnemi3, rotBombeEnnemi);
            yield return new WaitForSeconds(délaiEntreBombes);
            timerASbombe_now = 0;
            patternBombesSwitch = !patternBombesSwitch;
            SoundManager.PlayRandomOneShot(AudioExplosion, Explosions, 0.4f, 0.5f, 0.9f, 1f, 1f);
        }
    }

    void PatternLasers()
    {
        timerPatternLaser_now = timerPatternLaser_now + Time.deltaTime;
        timerLaserSafe_now = timerLaserSafe_now + Time.deltaTime;
        if (timerLaserSafe_now >= timerLaserSafe_max)
        {
            timerLaser_now = timerLaser_now + Time.deltaTime;
            if (!LaserSafeAppeared)
            {
                LaserSafeAppeared = true;
                Vector3 posZoneSafeEnnemi1 = new Vector3(spawnMainGauchePosition.x, spawnMainGauchePosition.y - 16, spawnMainGauchePosition.z);
                Vector3 posZoneSafeEnnemi2 = new Vector3(spawnMainDroitePosition.x, spawnMainDroitePosition.y - 16, spawnMainDroitePosition.z);
                Quaternion rotZoneSafeEnnemi1 = Quaternion.identity;
                Quaternion rotZoneSafeEnnemi2 = Quaternion.identity;
                GameObject zone1 = (GameObject)Instantiate(laserSafe, posZoneSafeEnnemi1, rotZoneSafeEnnemi1);
                GameObject zone2 = (GameObject)Instantiate(laserSafe, posZoneSafeEnnemi2, rotZoneSafeEnnemi2);
            }
            if (timerLaser_now >= timerLaser_max)
            {
                LaserSafeAppeared = false;
                timerLaserSafe_now = 0;
                timerLaser_now = 0;
                Vector3 posZoneEnnemi1 = new Vector3(spawnMainGauchePosition.x, spawnMainGauchePosition.y - 16, spawnMainGauchePosition.z);
                Vector3 posZoneEnnemi2 = new Vector3(spawnMainDroitePosition.x, spawnMainDroitePosition.y - 16, spawnMainDroitePosition.z);
                Quaternion rotZoneEnnemi1 = Quaternion.identity;
                Quaternion rotZoneEnnemi2 = Quaternion.identity;
                GameObject zone3 = (GameObject)Instantiate(laser, posZoneEnnemi1, rotZoneEnnemi1);
                GameObject zone4 = (GameObject)Instantiate(laser, posZoneEnnemi2, rotZoneEnnemi2);
            }
        }
    }

    IEnumerator StopPattern(float durée, string patternToStop)
    {
        yield return new WaitForSeconds(durée);
        switch (patternToStop)
        {
            case "PatternBalles":
                isPatternBalles = false;
                break;
            case "PatternZones":
                isPatternZones = false;
                break;
            case "PatternBombes":
                isPatternBombes = false;
                break;
            case "PatternLasers":
                isPatternLasers = false;
                break;
        }
        CallNewPattern();
    }

    void CallNewPattern()
    {
        int x = Random.Range(0, numberOfPatterns);
        print(x);
        if (CheckPattern(x) == true && canAttack == true)
        {
            switch (x)
            {
                case 0:
                    isPatternBalles = true;
                    cooldownPattern += duréePatternBalle;
                    lastPatternIdentifier = 0;
                    coroutine = StopPattern(duréePatternBalle, "PatternBalles");
                    StartCoroutine(coroutine);
                    break;
                case 1:
                    isPatternZones = true;
                    cooldownPattern += timerPatternZone_max;
                    lastPatternIdentifier = 1;
                    coroutine = StopPattern(timerPatternZone_max, "PatternZones");
                    StartCoroutine(coroutine);
                    break;
                case 2:
                    isPatternBombes = true;
                    cooldownPattern += timerPatternBombe_max;
                    lastPatternIdentifier = 2;
                    coroutine = StopPattern(timerPatternBombe_max, "PatternBombes");
                    StartCoroutine(coroutine);
                    break;
                case 3:
                    isPatternLasers = true;
                    cooldownPattern += timerPatternLaser_max;
                    lastPatternIdentifier = 3;
                    coroutine = StopPattern(timerPatternLaser_max, "PatternBombes");
                    StartCoroutine(coroutine);
                    break;
            }
        }
        else CallNewPattern();
    }

    bool CheckPattern(int selectedRandomPattern)
    {
        if (selectedRandomPattern == lastPatternIdentifier)
        {
            return false;
        }
        else return true;
    }

    void Chronometre()
    {
        timeSincePatternStarted += Time.deltaTime;
    }

    public override void TakeDamage(float damageReceived)
    {
        base.TakeDamage(damageReceived);
        bossHealthbar.transform.localScale = new Vector3(health / 100, 1f, 1f);
    }
}
