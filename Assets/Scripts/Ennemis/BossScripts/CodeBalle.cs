﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeBalle : MonoBehaviour {
    public float vitesse = 10;
    public float timerLifetimeBalle_max = 2f;
    public float timerAller = 0;
    public float timerRetour = 1f;
    public float timerLifetimeBalle_now = 0;
    bool isRetour = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timerLifetimeBalle_now += Time.deltaTime;
        //----------------------FAIRE AVANCER MA BALLE---------------------------------
        transform.position += transform.right * vitesse * Time.deltaTime;
        timerAller += Time.deltaTime;
        //--------------------------FAIRE REVENIR MA BALLE---------------------------------
        if (timerAller >= timerRetour && isRetour == false)
        {
            isRetour = true;
            if(transform.position.x < 0) CodeBoss.fireSecondWave = true;
            //vitesse = -vitesse;
            Quaternion rotation = Quaternion.Euler(0, 0, -(transform.rotation.z));
            transform.eulerAngles = new Vector3(0, 0, -(transform.eulerAngles.z));
            timerAller = 0;
        }
        //--------------------------DETRUIRE MA BALLE---------------------------------
        if (timerLifetimeBalle_now >= timerLifetimeBalle_max)
        {
            Destroy(gameObject);
            timerLifetimeBalle_now = 0;
        }

    }




    //-----------------QUAND MA BALLE ENTRE EN COLLISION------------------------------//
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Physics2D.IgnoreCollision(collision, GetComponent<Collider2D>());
            if (PlayerController.invincible == false)
            {
                collision.GetComponent<HealthScript>().TakeDamage();
            }
        }
        if (collision.gameObject.layer == 10)
        {
            print(collision.gameObject.name);
            Destroy(gameObject);
        }
    }
}
