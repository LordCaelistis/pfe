﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeBombe : MonoBehaviour
{
    public float vitesse = 30;
    public float timerStartBombe_max = 0.5f;
    public float timerStartBombe_now = 0f;
    public float timerLifetimeBombe_max = 2f;
    public float timerLifetimeBombe_now = 0;

    // Desired duration of the shake effect
    private float shakeDuration = 0f;
    // A measure of magnitude for the shake. Tweak based on your preference
    public float shakeMagnitude = 0.02f;
    // A measure of how quickly the shake effect should evaporate
    public float shakeFadeOut = 1.0f;
    // The initial position of the GameObject

    Vector3 positionDébutShake;
    // Use this for initialization
    void Start()
    {
        positionDébutShake = transform.position;
        GetComponent<Collider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(timerStartBombe_now >= timerStartBombe_max)
        {
            GetComponent<Collider2D>().enabled = true;
            timerLifetimeBombe_now = timerLifetimeBombe_now + Time.deltaTime;
            transform.localScale = transform.localScale + new Vector3(0.2f, 0.2f, 0) * vitesse * Time.deltaTime;
        }
        else transform.localPosition = positionDébutShake + Random.insideUnitSphere * shakeMagnitude;

        //--------------------------DETRUIRE MA BOMBE---------------------------------
        if (timerLifetimeBombe_now >= timerLifetimeBombe_max)
        {
            Destroy(gameObject);
            timerLifetimeBombe_now = 0;
        }

        timerStartBombe_now += Time.deltaTime;

    }




    //-----------------QUAND MA BOMBE ENTRE EN COLLISION------------------------------//
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if(PlayerController.invincible == false)
            {
                collision.GetComponent<HealthScript>().TakeDamage();
            }            
        }
    }
}