﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeZone : MonoBehaviour {
    float timerLifetimeZone_now = 0;
    float timerLifetimeZone_max = 2f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timerLifetimeZone_now = timerLifetimeZone_now + Time.deltaTime;
        //--------------------------DETRUIRE MA ZONE---------------------------------
        if (timerLifetimeZone_now >= timerLifetimeZone_max)
        {
            Destroy(gameObject);
            timerLifetimeZone_now = 0;
        }
    }

    //-----------------QUAND MA ZONE ENTRE EN COLLISION------------------------------//
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (PlayerController.invincible == false)
            {
                collision.GetComponent<HealthScript>().TakeDamage();
            }
        }
    }
}
