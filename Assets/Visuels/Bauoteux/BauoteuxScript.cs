﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BauoteuxScript : MonoBehaviour {

    public GameObject protagoniste;
    public GameObject targetPosition;
    public GameObject tirBlaster;
    public Vector2 direction;

    public SpriteRenderer body;
    public Animator animator;

    public int speed = 5;
    public float delay = 0.3f;

    // Use this for initialization
    void Start () {
        protagoniste = GameObject.Find("Protagoniste");
    }
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.position = targetPosition.transform.position;

        direction = GetCursorRelativeVector();
        animator.SetFloat("directionX", direction.x);
        animator.SetFloat("directionY", direction.y);
    }

    //Fonctions mathématiques pour repérer position du curseur et viser correctement
    Vector2 GetCursorRelativeVector()
    {
        Vector3 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float relativeX = cursorPos.x - transform.position.x;
        float relativeY = cursorPos.y - transform.position.y;
        return new Vector2(relativeX, relativeY);
    }
    float GetCursorRelativeAngle()
    {
        Vector2 relativeVector = GetCursorRelativeVector();
        return Mathf.Atan2(relativeVector.y, relativeVector.x) * Mathf.Rad2Deg;
    }

    public void Shoot()
    {
        protagoniste.GetComponent<EnergyScript>().energy = protagoniste.GetComponent<EnergyScript>().energy - 1;
        protagoniste.GetComponent<EnergyScript>().beginEnergyRegen();

        //Récupération de la position de la souris par rapport à l'écran
        Vector3 pz = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,
                        Input.mousePosition.y, Camera.main.transform.position.y - transform.position.y));
        Vector3 direction = pz - transform.position;

        direction.z = 0;
        //Normalize() impose une longueur de 1 au vecteur. Sinon, la vitesse du projectile est modifiée
        //selon que la souris soit plus ou moins loin
        direction.Normalize();
        Vector3 pos0 = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
        Quaternion rot0 = Quaternion.Euler(0, 0, GetCursorRelativeAngle() - 90);
        GameObject projectile = Instantiate(tirBlaster, pos0, rot0);
        projectile.GetComponent<BulletBehavior>().SetBulletDamage(PlayerController.blasterDamage);
        projectile.GetComponent<Rigidbody2D>().velocity = direction * PlayerController.tirBlasterSpeed;
    }
}
