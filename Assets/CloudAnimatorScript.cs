﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudAnimatorScript : MonoBehaviour {

    public GameObject deathPosition;
    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.position -= new Vector3(speed * Time.deltaTime, 0, 0);

        if (gameObject.transform.position.x < deathPosition.transform.position.x)
            Destroy(gameObject);
	}
}
